import 'package:xposure/httpUtils/api.dart';

class FeedVideoService {
  ApiProvider apiProvider = ApiProvider();
  getFeedVideoRecords(String userId,int pageNo)async{
    var a = await apiProvider.getFeedVideos(userId, pageNo);
    return a;
  }

  doAction(response){
    apiProvider.feedResponse(response);
  }
}