import 'dart:async';
import 'dart:io';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:logger/logger.dart';
import 'package:xposure/ui/videoPreviwer.dart';
import 'package:xposure/utils/genericfun.dart';
import 'package:path_provider/path_provider.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:xposure/blocs/cameraBloc.dart';
import 'package:lamp/lamp.dart';
import 'package:xposure/constants/strings.dart' as Constants;
import 'package:xposure/constants/images.dart' as IMAGES;
import 'package:xposure/constants/methods.dart';

class VideoRecorderExample extends StatefulWidget {
  @override
  _VideoRecorderExampleState createState() {
    return _VideoRecorderExampleState();
  }
}

class _VideoRecorderExampleState extends State<VideoRecorderExample>
    with WidgetsBindingObserver {
  CameraController controller;
  CameraBloc cameraBloc;
  String videoPath;
  static const platform = const MethodChannel(Constants.VIDEO_STREAM);
  static const tokenPlatform = const MethodChannel(Constants.USERID);
  static const refreshPlatform =
      const MethodChannel(Constants.REFRESH_TAPS_PAGE);
  String userId = "";
  Stopwatch watch = Stopwatch();
  Timer timer;
  bool startStop = true;
  bool video = true;
  String elapsedTime = '';
  List<CameraDescription> cameras;
  int selectedCameraIdx;
  String duration;
  bool _hasFlash = false;
  bool _isOn = false;
  double _intensity = 1.0;
  bool permissionGranted = false;
  final logger = Logger();

  Future<dynamic> nativeMethodCallHandler(MethodCall methodCall) async {
    logger.d('methodCall');
    switch (methodCall.method) {
      case 'refreshTaps':
        print(">>>>>>>>>>Successfully loaded");
        // Navigator.pop(context);
//        setState(() {
//        });
        return;
      default:
    }
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // App state changed before we got the chance to initialize.
    switch (state) {
      case AppLifecycleState.resumed:
        print(">>>>>IAM HERE");
        break;
      case AppLifecycleState.inactive:
        // TODO: Handle this case.
        break;
      case AppLifecycleState.paused:
        print(">>>>>IAM HERE,>>>>>>-----PAUSED");
        controller?.dispose();
        // TODO: Handle this case.
        break;
      case AppLifecycleState.detached:
        // TODO: Handle this case.
        break;
    }
    if (controller == null || !controller.value.isInitialized) {
      return;
    }
    if (state == AppLifecycleState.inactive) {
      controller?.dispose();
    } else if (state == AppLifecycleState.resumed) {
      if (controller != null) {
        _onCameraSwitched(controller.description);
      }
    }
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    refreshPlatform.setMethodCallHandler(nativeMethodCallHandler);
    // Get the listonNewCameraSelected of available cameras.
    // Then set the first camera as selected.
    availableCameras().then((availableCameras) {
      cameras = availableCameras;
      if (cameras.length > 0) {
        setState(() {
          selectedCameraIdx = 0;
        });
        user();
        _onCameraSwitched(cameras[selectedCameraIdx]).then((void v) {});
      }
    }).catchError((err) {
      logger.e('Error: $err.code\nError Message: $err.message');
    });

    //  initPlatformState();
    print(">>>>find");
  }

  //getting user id
  user() async {
    userId = getUserId();
  }

  initPlatformState() async {
    bool hasFlash = await Lamp.hasLamp;
    logger.d("Device has flash ? $hasFlash");
    setState(() {
      _hasFlash = hasFlash;
    });
  }

//updated timer duration
  updateTime(Timer timer) {
    if (watch.isRunning) {
      setState(() {
        elapsedTime = transformMilliSeconds(watch.elapsedMilliseconds);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    cameraBloc = CameraBloc();
    return Scaffold(
        body: Column(children: <Widget>[
      Expanded(
        child: Stack(
          children: <Widget>[
            Center(
              child: _cameraPreviewWidget(),
            ),
            Positioned(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Container(
                          child: Text(elapsedTime,
                              style: TextStyle(
                                  fontSize: 25.0, color: Colors.black)),
                        )
                      ]),
                  Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Visibility(
                            visible: video,
                            child: Container(
                              decoration: BoxDecoration(
                                  color: Colors.white24,
                                  shape: BoxShape.circle),
                              child: IconButton(
                                iconSize: 8,
                                icon: new Image.asset(IMAGES.FLASH),
                                onPressed: () {
                                  //TODO implement
                                  // turnFlash();
                                },
                              ),
                            )),
                        SizedBox(
                          width: MediaQuery.of(context).size.width * 0.03,
                        ),
                        Container(
                          child: IconButton(
                            iconSize: 70,
                            icon: controller != null &&
                                    controller.value.isRecordingVideo
                                ? new Image.asset(IMAGES.STOP)
                                : new Image.asset(
                                    IMAGES.RECORD,
                                  ),
                            onPressed: () {
                              controller != null &&
                                      controller.value.isInitialized &&
                                      !controller.value.isRecordingVideo
                                  ? _onRecordButtonPressed()
                                  : _onStopButtonPressed();
                            },
                          ),
                        ),
                        Visibility(
                            visible: video,
                            child: IconButton(
                                iconSize: 40,
                                icon: new Image.asset(
                                  IMAGES.SWITCH_CAMERA,
                                ),
                                onPressed: () {
                                  _onSwitchCamera();
                                })),
                      ],
                    ),
                  ),
                ])),
            Positioned(
                child: Visibility(
              visible: video,
              child: Container(
                child: IconButton(
                  iconSize: 30,
                  icon: new Image.asset(IMAGES.BACK),
                  onPressed: () {
                    cameraBloc.goToPreviewScreen(context);
                  },
                ),
              ),
            ))
          ],
        ),
      )
    ]));
  }

  // Display 'Loading' text when the camera is still loading.
  Widget _cameraPreviewWidget() {
    if (controller == null || !controller.value.isInitialized) {
      return Container(
          color: Colors.black,
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Center(child: CircularProgressIndicator()));
      // Text(
      //   Constants.LOADING,
      //   style: TextStyle(
      //     color: Colors.white,
      //     fontSize: 20.0,
      //     fontWeight: FontWeight.w900,
      //   ),
      // );
    }

    return Container(
      color: Colors.black,
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Transform.scale(
          scale: controller.value.aspectRatio /
              (MediaQuery.of(context).size.width /
                  MediaQuery.of(context).size.height),
          child: AspectRatio(
            aspectRatio: controller.value.aspectRatio,
            child: CameraPreview(controller),
          )),
    );
  }

  String timestamp() => DateTime.now().millisecondsSinceEpoch.toString();

  Future<void> _onCameraSwitched(CameraDescription cameraDescription) async {
    if (controller != null) {
      await controller.dispose();
    }

    controller = CameraController(cameraDescription, ResolutionPreset.high);

    // If the controller is updated then update the UI.
    controller.addListener(() {
      if (mounted) {
        setState(() {});
      }

      if (controller.value.hasError) {
        Fluttertoast.showToast(
            msg: 'Camera error ${controller.value.errorDescription}',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIos: 1,
            backgroundColor: Colors.red,
            textColor: Colors.white);
      }
    });

    try {
      await controller.initialize();
    } on CameraException catch (e) {
      controller.dispose();
      await controller.initialize();
      setState(() {});
      _showCameraException(e);
    }

    if (mounted) {
      setState(() {});
    }
  }

//turn on off the mobile flash
  Future turnFlash() async {
    _isOn ? Lamp.turnOff() : Lamp.turnOn(intensity: _intensity);
    var f = await Lamp.hasLamp;
    setState(() {
      _hasFlash = f;
      _isOn = !_isOn;
    });
  }

//for switching the front and back cameras
  void _onSwitchCamera() {
    selectedCameraIdx =
        selectedCameraIdx < cameras.length - 1 ? selectedCameraIdx + 1 : 0;
    CameraDescription selectedCamera = cameras[selectedCameraIdx];

    _onCameraSwitched(selectedCamera);

    setState(() {
      selectedCameraIdx = selectedCameraIdx;
    });
  }

  void _onRecordButtonPressed() {
    _startVideoRecording().then((String filePath) {
      if (filePath != null && controller.value.isRecordingVideo) {
        leftButtonPressed();
        Fluttertoast.showToast(
            msg: Constants.RECORDING_VIDEO_STARTED,
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.CENTER,
            timeInSecForIos: 1,
            backgroundColor: Colors.grey,
            textColor: Colors.white);
        setState(() {
          video = false;
        });
      }
    });
  }

//to start stop the timer
  void leftButtonPressed() {
    if (startStop) {
      startWatch();
    } else {
      stopWatch();
    }
  }

  startWatch() {
    setState(() {
      startStop = false;
      watch.start();
      timer = Timer.periodic(Duration(milliseconds: 100), updateTime);
    });
  }

  stopWatch() {
    duration = elapsedTime;
    logger.d(">>>>>>---NOWTIME--->>>$duration");
    setState(() {
      elapsedTime = '';
      watch.reset();
      startStop = true;
      watch.stop();
    });
  }

//transform milliseconds from updated time method and converted as hours minutes seconds
  transformMilliSeconds(int milliseconds) {
    int hundreds = (milliseconds / 10).truncate();
    int seconds = (hundreds / 100).truncate();
    int minutes = (seconds / 60).truncate();
    int hours = (minutes / 60).truncate();

    String hoursStr = (hours % 60).toString().padLeft(2, '0');
    String minutesStr = (minutes % 60).toString().padLeft(2, '0');
    String secondsStr = (seconds % 60).toString().padLeft(2, '0');

    return "$hoursStr:$minutesStr:$secondsStr";
  }

//when camera was stoped navigate to peview screen
  void _onStopButtonPressed() async {
    leftButtonPressed();
    setState(() {
      video = true;
    });
    _stopVideoRecording().then((_) async {
      if (mounted) setState(() {});
      pageRouter(context, VideoPreviewer(videoPath, userId, duration));
      logger.d(">>>>>>>>-----videoPath-->>$videoPath");
    });
  }

  Future<String> _startVideoRecording() async {
    if (!controller.value.isInitialized) {
      Fluttertoast.showToast(
          msg: Constants.PLEASE_WAIT,
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 1,
          backgroundColor: Colors.grey,
          textColor: Colors.white);

      return null;
    }

    // Do nothing if a recording is on progress
    if (controller.value.isRecordingVideo) {
      return null;
    }

    final Directory appDirectory = await getApplicationDocumentsDirectory();
    final String videoDirectory = '${appDirectory.path}/Videos';
    await Directory(videoDirectory).create(recursive: true);
    final String currentTime = DateTime.now().millisecondsSinceEpoch.toString();
    final String filePath = '$videoDirectory/${currentTime}.mp4';

    try {
      await controller.startVideoRecording(filePath);
      videoPath = filePath;
    } on CameraException catch (e) {
      _showCameraException(e);
      return null;
    }

    return filePath;
  }

  Future<void> _stopVideoRecording() async {
    if (!controller.value.isRecordingVideo) {
      return null;
    }

    try {
      await controller.stopVideoRecording();
    } on CameraException catch (e) {
      _showCameraException(e);
      return null;
    }
  }

  void _showCameraException(CameraException e) {
    String errorText = 'Error: ${e.code}\nError Message: ${e.description}';
    logger.e(errorText);

    Fluttertoast.showToast(
        msg: 'Error: ${e.code}\n${e.description}',
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIos: 1,
        backgroundColor: Colors.red,
        textColor: Colors.white);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    print(">>>>>>>>>>>CAMERA>>>>>>disposed");
    controller.dispose();
  }
}
