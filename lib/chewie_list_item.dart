import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';
class ChewieListItem extends StatefulWidget {
  // This will contain the URL/asset path which we want to play
  final VideoPlayerController videoPlayerController;
  final bool looping;
  final String url;
  

  ChewieListItem({
    @required this.videoPlayerController,
    @required this.url,
    @required this.looping,
    Key key,
  }) : super(key: key);

  @override
  _ChewieListItemState createState() => _ChewieListItemState();
}

class _ChewieListItemState extends State<ChewieListItem> {
  ChewieController _chewieController;
  bool isPending=true;

  @override
  void initState() {
    super.initState();
   //widget.videoPlayerController.seekTo(Duration(seconds:1));
    // Wrapper on top of the videoPlayerController
    print(widget.url);
    _chewieController = ChewieController(

      videoPlayerController: widget.videoPlayerController..initialize(

      ),
      aspectRatio: widget.videoPlayerController.value.aspectRatio,
      // Prepare the video to be played and display the first frame
      autoInitialize: isPending,
      autoPlay: true,
      looping: widget.looping,
      // Errors can occur for example when trying to play a video
      // from a non-existent URL
      errorBuilder: (context, errorMessage) {
        return Center(
          child: Text(
            errorMessage,
            style: TextStyle(color: Colors.white),
          ),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Chewie(
        controller: _chewieController,
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    // IMPORTANT to dispose of all the used resources
    //widget.videoPlayerController.dispose();
    //_chewieController.dispose();
  }
}