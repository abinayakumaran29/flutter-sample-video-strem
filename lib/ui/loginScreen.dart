import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:xposure/blocs/loginBloc.dart';
import 'package:xposure/ui/splashScreen.dart';
import 'package:xposure/main.dart';
import 'package:xposure/constants/strings.dart' as Constants;

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _LoginScreen();
  }
}

class _LoginScreen extends State<LoginScreen> {
  LoginBloc loginBloc;
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    loginBloc = LoginBloc(); 
    return Scaffold(
        body:userId !=null ?SplashScreen() : Center(
      child: OutlineButton(
        child: Text(Constants.LOGIN_WITH_GOOGLE),onPressed: (){
          loginBloc.gotoNextScreen(context: context);
        },
      ),
    ));
  }
}
