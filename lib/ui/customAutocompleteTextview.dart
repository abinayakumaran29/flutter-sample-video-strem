import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:logger/logger.dart';
import 'package:xposure/httpUtils/api.dart';

class Tags{
static List tags = List();
}

class AutoCompleteTextView extends StatefulWidget {
  List<String> tags;
  AutoCompleteTextView({this.tags});
  @override
  State<StatefulWidget> createState() {
    tags = List<String>();
    // tags.add("hello");
    // tags.add("hii");
    // tags.add("hello");
    // tags.add("hii");
    //tags.add("jjj"); // TODO: implement createState
    return _AutoCompleteText();
  }
}

class _AutoCompleteText extends State<AutoCompleteTextView> {
  int c, b;
  int colnum;
  String suggestions = "";
  bool genrateTagList = false;
  List<String> filtered = List();
  List userTags = List();
  var tag = [];
  TextEditingController textEditingController = TextEditingController();
  StreamController _stream = StreamController.broadcast();
  StreamController _stream1 = StreamController.broadcast();
  ApiProvider apiProvider = ApiProvider();
final logger = Logger();

  getfilterlist(String value) async {
    //tag = alpha.where((a) => value == a).toList();
    var tag = await apiProvider.hashtagList(value);
    return tag;
  }
  @override
  void initState() {
    // TODO: implement initState
    Tags.tags.clear();
    super.initState();
  }

  var focusNode = new FocusNode();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return
        //Scaffold(
        //body:
        StreamBuilder(
      stream: _stream1.stream,
      initialData: widget.tags,
      builder: (context, snapshot) {
        if (snapshot.data != null) {
          colnum = getColumNum(snapshot.data.length);
          print(">>>>SNP>>>>$snapshot");
        }
        // return RawMaterialButton(
        //     child:
        if (genrateTagList) {
          tag.clear();
          FocusScope.of(context).requestFocus(focusNode);
        }
        return !genrateTagList
            ? showHashTagList(snapshot)
            : SingleChildScrollView(
                child: StreamBuilder(initialData: [],
                    stream: _stream.stream,
                    builder: (contex, snapsht) {
                      return Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                         Padding(child: TextField(
                            focusNode: focusNode,
                            // decoration:
                            //     InputDecoration(border: InputBorder.none),
                            controller: textEditingController,
                            onChanged: (value) {
                              print(">>>>ONCHANGED$value");
                              if (value.contains(" ")) {
                                if(value.trim().length!=0){
                                  FocusScope.of(context).requestFocus(new FocusNode());
                                widget.tags.add(value);
                                Tags.tags.add(value);
                                textEditingController.clear();
                                genrateTagList = false;
                                _stream1.sink.add(widget.tags);
                                }
                              } else {
                                //getfilterlist(value);
                                suggestions = value;
                                _stream.sink.add("");
                              }
                            },
                          ),padding: const EdgeInsets.only(right: 10,left: 10),),
                          Container(
                            constraints: BoxConstraints(
                              minHeight: MediaQuery.of(context).size.height * 0.05 ,maxHeight:MediaQuery.of(context).size.height * 0.20),
                              child: suggestions.trim().length!=0 ? FutureBuilder(
                                  future: apiProvider.hashtagList(suggestions),
                                  builder: (context, snaps) {
                                    logger.d(">>>>>>>>>>>TAGS---->${snaps.data}");
                                    if(snaps.data !=null){
                                      tag = snaps.data;
                                    }
                                    return snaps.connectionState ==
                                                ConnectionState.done &&
                                            snaps.data != null
                                        ? ListView.builder(
                                            shrinkWrap: true,
                                            itemCount: tag.length,
                                            itemBuilder: (context, index) {
                                              return ListTile(
                                                onTap: () {
                                                  if(!Tags.tags.contains(tag[index])){
                                                  widget.tags.add(tag[index]);
                                                  Tags.tags.add(tag[index]);
                                                  textEditingController.text =
                                                      tag[index];
                                                  textEditingController.clear();
                                                  genrateTagList = false;
                                                  _stream1.sink
                                                      .add(widget.tags);
                                                  }
                                                      FocusScope.of(context).requestFocus(new FocusNode());
                                                },
                                                title: Text(
                                                  tag[index],
                                                ),
                                              );
                                            },
                                          )
                                        : Center(
                                            child: CircularProgressIndicator(),
                                          );
                                  }):Container()),showHashTagList(snapshot)
                        ],
                      );
                    }));
      },
      //)
    );
  }

  generateHashTag() {}

  showHashTagList(snapshot) {
    //snapshot.data = widget.tags;

    return GestureDetector(
      child: snapshot.data.length == 0 
          ? Container(
              child: !genrateTagList ? TextField(
                readOnly: true,
                decoration: InputDecoration(hintText: "Add HashTags"),
                onTap: () {
                  genrateTagList = true;
                  _stream1.sink.add(widget.tags);
                },
              ):Container(),
            )
          :
          // GridView.builder(
          //     gridDelegate:
          //         SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2,),
          //     itemCount: widget.tags.length,
          //     itemBuilder: (context, index) {
          //       return
          //       hashTagWidget(alpha[index]);
          //     },
          //     //)
          //   ),

          RawMaterialButton(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  for (int a = 0; a < colnum; a++)
                    Padding(
                        child: makeRowWs(a, snapshot),
                        padding: const EdgeInsets.only(bottom: 10, top: 10)),
                ],
              ),
              onPressed: () {
                genrateTagList = true;
                _stream1.sink.add(widget.tags);
              },
            ),
      onTap: () {
        print(">>>>>>>GESTURECLICKED");
        genrateTagList = true;
        _stream1.sink.add(widget.tags);
      },
    );
  }

  getColumNum(int a) {
    print(">>>>>>>>>TOTAL>>-$a");
    var v = a / 3;
    print(">>>>>>-v---$v");
    b = a % 3;
    print(">>>>>>--b--$b");
    v = b != 0 ? v + 1 : v + 0;
    print(">>>>>>--v2--$v");
    c = int.parse(v.toString().split(".")[0]);
    print(">>>>>>--columnnum---$c");
    return c;
  }

  makeRowWs(index, snapshot) {
    print(">>>>>>>WWWWW>>>$c>>>>>---$b-----");
    int rowlength = index == (c - 1) ? b : 3;
    if (rowlength == 0) {
      rowlength = b == 0 ? 3 : b;
    }
    return rowlength == 3
        ? responsiveRow(rowlength, snapshot, index)
        : Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              for (int a = 0; a < rowlength; a++)
                // Padding(
                //child:
                hashTagWidget(snapshot.data[objectIndexFounder(index, a)]),
              // padding: const EdgeInsets.only(right: 10, left: 10)),
            ],
          );
  }

  responsiveRow(int rowlength, snapshot, index) {
    var maxlenth = false;
    for (int a = 0; a < rowlength; a++) {
      if (snapshot.data[objectIndexFounder(index, a)].length > 10) {
        maxlenth = true;
      }
    }

    if (maxlenth) {
      print(">>>>>>>>>LENGTHTOOBIG");
      return Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Padding(
                child:
                    hashTagWidget(snapshot.data[objectIndexFounder(index, 0)]),
                padding: const EdgeInsets.only(top: 10, bottom: 10),
              )
            ],
          ),
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              for (int z = 1; z < 3; z++)
                Padding(
                  child: hashTagWidget(
                      snapshot.data[objectIndexFounder(index, z)]),
                  padding: const EdgeInsets.only(top: 10, bottom: 10),
                )
            ],
          )
        ],
      );
    } else {
      return Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          for (int a = 0; a < rowlength; a++)
            // Padding(
            //child:
            hashTagWidget(snapshot.data[objectIndexFounder(index, a)]),
          // padding: const EdgeInsets.only(right: 10, left: 10)),
        ],
      );
    }
  }

  int objectIndexFounder(columnindex, rowindex) {
    int p = columnindex * 3;
    int i = p + rowindex;
    print(">>>>>>>>>>INDEXXXXXXXX>>>>>>>>$i");
    return i;
  }

  hashTagWidget(String tags) {
    //a = b!=0?(a.round()+1):0;

    // print(">>>>>>>>>${getColumNum(10)}>>>>>>");
    return Container(
      height: 30,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: Colors.grey,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Padding(
            child: Text(
              tags,
              style: TextStyle(
                color: Colors.white,
              ),
            ),
            padding: const EdgeInsets.only(left: 10),
          ),
          // Padding(
          //child:
          IconButton(
            color: Colors.white,
            iconSize: 20,
            icon: Icon(
              Icons.clear,
            ),
            onPressed: () {
              //widget.tags.indexOf(tags);
              widget.tags.remove(tags);

              Tags.tags.remove(tags);
              _stream1.sink.add(widget.tags);
              print(">>>>>>>>INDEXOF${tags}");
            },
          ),
          //padding: const EdgeInsets.only(right: 10,),
          // )
        ],
      ),
    );
  }
}
