import 'dart:async';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:xposure/api/api.dart';
import 'package:xposure/constants/methods.dart';
import 'package:xposure/httpUtils/api.dart';
import 'package:xposure/blocs/historyVideoBloc.dart';
import 'package:preload_page_view/preload_page_view.dart';
import 'package:xposure/constants/strings.dart' as Constants;
import 'package:xposure/ui/singleVideoPlayer.dart';
import 'package:xposure/utils/DateUtil.dart';

class HistoryVideo extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createStatejkekfj
    return _HistoryideoScreen();
  }
}

class _HistoryideoScreen extends State<HistoryVideo>
    with TickerProviderStateMixin {
  var likeData;
  var uploadData;
  var historyData;
  HistoryBloc historyVideoBloc;
  TabController _tabController;
  ScrollController videoPlayController;
  PreloadPageController preLoadpageController;
  ApiProvider apiProvider = ApiProvider();
  Widget videoList;
  var fetcher;
  _handleTabSelection() {
    print(_tabController.index);
  }

  @override
  void initState() {
    _tabController = new TabController(vsync: this, length: 3);
    _tabController.addListener(_handleTabSelection);
    // apiList = apiProvider.historyVideos();
    super.initState();
  }

  @override
  void dispose() {
    historyVideoBloc.dispose();
    print(">>>>>>>-----DISPOSE METHOD CALLED");
    StaticNeeds.disposed();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    historyVideoBloc = HistoryBloc();

    // TODO: implement build
    return new DefaultTabController(
      length: 3,
      child: new Scaffold(
        appBar: AppBar(
            backgroundColor: Colors.white,
            flexibleSpace: FlexibleSpaceBar(
              centerTitle: true,
              title: Padding(
                padding: const EdgeInsets.only(bottom: 35.0),
                child: Container(
                    decoration: BoxDecoration(
                      border: Border(
                          bottom: BorderSide(color: Colors.black26, width: 2)),
                    ),
                    child: Stack(children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Container(
                                height:
                                    MediaQuery.of(context).size.height * 0.20,
                                width: MediaQuery.of(context).size.width * .30,
                                child: Image.network(
                                  ApplicationConstants.photoUrl,
                                ),
                              ),
                            ],
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                ApplicationConstants.userName,
                                style: TextStyle(
                                    fontSize: 20, color: Colors.black),
                              ),
                              Text(
                                "86k Followers",
                                style: TextStyle(
                                    fontSize: 20, color: Colors.black),
                              ),
                              Text(
                                "Popularity:50",
                                style: TextStyle(
                                    fontSize: 20, color: Colors.black),
                              ),
                            ],
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.all(5.0),
                                child: SizedBox(
                                  width:
                                      MediaQuery.of(context).size.width * 0.35,
                                  child: RaisedButton(
                                    child: Text(
                                      'Edit Profile',
                                      style: TextStyle(color: Colors.white),
                                    ),
                                    color: Colors.pinkAccent,
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(16.0))),
                                    onPressed: () {},
                                  ),
                                ),
                              )
                            ],
                          ),
                        ],
                      )
                    ])),
              ),
            ),
            bottom: PreferredSize(
              preferredSize: const Size.fromHeight(150.0),
              child: Padding(
                padding: const EdgeInsets.all(0.0),
                child: new TabBar(
                  controller: _tabController,
                  tabs: <Widget>[
                    new Tab(
                        child: Text('Uploads',
                            style: TextStyle(color: Colors.black))),
                    new Tab(
                        child: Text('Liked',
                            style: TextStyle(color: Colors.black))),
                    new Tab(
                      child: Text('History',
                          style: TextStyle(color: Colors.black)),
                    ),
                  ],
                ),
              ),
            )),
        body: Stack(children: <Widget>[
          TabBarView(
              controller: _tabController,
              children: <Widget>[DynamicTab(0), DynamicTab(1), DynamicTab(2)])
        ]),
      ),
    );
  }
}

class DynamicTab extends StatefulWidget {
  int tabIndex;
  DynamicTab(this.tabIndex);
  @override
  State<StatefulWidget> createState() {
    return like();
  }
}

class like extends State<DynamicTab> {
  HistoryBloc historyBloc = HistoryBloc();
  String screenName = "No Name";
  ApiProvider apiProvider = ApiProvider();
  var stream;
  ScrollController _scrollController = ScrollController();
  TabBarCommonBloc tabBarCommonBloc;
  GlobalKey<ScaffoldState> scaffoldstate = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    tabBarCommonBloc = TabBarCommonBloc();
    tabBarCommonBloc.initState(context);
    tabBarCommonBloc.tabIndex = widget.tabIndex;
    stream = tabBarCommonBloc.uploadDataStream;
    tabBarCommonBloc.getCurrentScreenInitialData(widget.tabIndex);
    // TODO: implement initState

    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        print(">>>>>>>------REACHES END");
        tabBarCommonBloc.scaffold = scaffoldstate;
        tabBarCommonBloc.uploadStreamSink.add(LoadMore());
      }
    });
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    tabBarCommonBloc.dispose();
    super.dispose();
  }

  Future<void> refreshPage() async {
    if (widget.tabIndex == 0) {
      StaticNeeds.uploadScreenPageNo = 1;
      StaticNeeds.uploadScreendata = [];
      StaticNeeds.isUploadScreenDatafetched = false;
    } else if (widget.tabIndex == 1) {
      StaticNeeds.likeScreenPageNo = 1;
      StaticNeeds.isLikedScreenDatafetched = false;
      StaticNeeds.likeScreendata = [];
    } else if (widget.tabIndex == 2) {
      StaticNeeds.histroyScreenPageNo = 1;
      StaticNeeds.historyScreendata = [];
      StaticNeeds.isHistoryScreenDatafetched = false;
    }
    await Future.delayed(Duration(seconds: 3));
    tabBarCommonBloc.getCurrentScreenInitialData(widget.tabIndex);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: scaffoldstate,
      body: RefreshIndicator(
        onRefresh: refreshPage,
        child: StreamBuilder(
            stream: stream,
            //initialData: StaticNeeds.uploadScreendata,
            builder: (context, snapShot) {
              logger.d('-----HISTORY----${snapShot}');
              return snapShot.connectionState == ConnectionState.waiting
                  ? Center(
                      child: CircularProgressIndicator(),
                    )
                  :  ListView.builder(
                        shrinkWrap: true,
                        itemCount: snapShot.data.length,
                        scrollDirection: Axis.vertical,
                        controller: _scrollController,
                        itemBuilder: (BuildContext context, int index) {
                          return GestureDetector(
                            child: Card(
                              //padding: const EdgeInsets.all(7.0),
                              child: Container(
                                // decoration: new BoxDecoration(
                                //     border: new Border.all(width: 0.5, color: Colors.white)),
                                child: Row(
                                  children: <Widget>[
                                    Container(
                                      decoration: new BoxDecoration(
                                          border: new Border.all(
                                              width: 1.0, color: Colors.black),
                                          color: Colors.grey),
                                      height:
                                          MediaQuery.of(context).size.height *
                                              0.14,
                                      width: MediaQuery.of(context).size.width *
                                          0.45,
                                      child: Stack(children: <Widget>[
                                        Center(
                                          child: new Icon(
                                            Icons.play_circle_outline,
                                            color: Colors.black,
                                            size: 30,
                                          ),
                                        ),
                                        Positioned(
                                          bottom: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              0.01,
                                          right: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              0.03,
                                          child: Text(
                                            snapShot.data[index]['duration'] !=
                                                    null
                                                ? snapShot.data[index]
                                                    ['duration']
                                                : '00:00:00',
                                            style:
                                                TextStyle(color: Colors.white),
                                          ),
                                        )
                                      ]),
                                    ),
                                    Container(
                                        height:
                                            MediaQuery.of(context).size.height *
                                                0.14,
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.50,
                                        child: Padding(
                                          padding:
                                              const EdgeInsets.only(left: 10.0),
                                          child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceEvenly,
                                              children: <Widget>[
                                                Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: <Widget>[
                                                    Expanded(
                                                      child: Text(
                                                        snapShot.data[index][
                                                                    'videoTitle'] !=
                                                                null
                                                            ? snapShot
                                                                    .data[index]
                                                                ['videoTitle']
                                                            : screenName,
                                                        style: TextStyle(
                                                            fontSize: 18.0),
                                                        maxLines: 2,
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                Row(
                                                  children: <Widget>[
                                                    Text(
                                                      DateUtil().formattedDate(
                                                          DateTime.parse(snapShot
                                                                  .data[index]
                                                              ['createdAt'])),
                                                      style: TextStyle(
                                                          fontSize: 12.0),
                                                    ),
                                                  ],
                                                ),
                                                Row(
                                                  children: <Widget>[
                                                    Text(
                                                      "100 Likes",
                                                      style: TextStyle(
                                                          fontSize: 14.0),
                                                    ),
                                                  ],
                                                )
                                              ]),
                                        ))
                                  ],
                                ),
                              ),
                            ),
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => SingleVideoPlayer(
                                              snapShot.data[index],)));
                            },
                          );
                        },
                        // itemCount: myList.length + 1,
                    );
            }),
      ),
    );
  }
}

// class DisLikeView extends StatefulWidget {
//   @override
//   State<StatefulWidget> createState() {
//     return dislike();
//   }
// }

// class dislike extends State<DisLikeView> {
//   HistoryBloc historyBloc = HistoryBloc();
//   String screenName;
//   ApiProvider apiProvider = ApiProvider();
//   var liked;
//   List myList;
//   ScrollController _scrollController = ScrollController();
//   int _currentMax = 10;
//   int currentindex = 1;
//   @override
//   void initState() {
//     liked = apiProvider.getLikedVideos(ApplicationConstants.userId);
//     // TODO: implement initState
//     super.initState();
//     _scrollController.addListener(() {
//       if (_scrollController.position.pixels ==
//           _scrollController.position.maxScrollExtent) {
//         _getMoreData();
//       }
//     });
//   }

//   _getMoreData() {
//     logger.w('----pagination------$currentindex');
//     // upload = apiProvider.getUploadVideos(ApplicationConstants.userId,currentIndex+1);
//     _currentMax = _currentMax + 10;
//     setState(() {});
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         body: FutureBuilder(
//             future: liked,
//             builder: (context, snapShot) {
//               print('-----22222----${snapShot.data}');
//               return snapShot.connectionState == ConnectionState.waiting
//                   ? Center(
//                       child: CircularProgressIndicator(),
//                     )
//                   : ListView.builder(
//                       shrinkWrap: true,
//                       itemCount: snapShot.data.length,
//                       scrollDirection: Axis.vertical,
//                       itemBuilder: (BuildContext context, int index) {
//                         return GestureDetector(
//                           child: Card(
//                             //padding: const EdgeInsets.all(7.0),
//                             child: Container(
//                               // decoration: new BoxDecoration(
//                               //     border: new Border.all(width: 0.5, color: Colors.white)),
//                               child: Row(
//                                 children: <Widget>[
//                                   Container(
//                                     decoration: new BoxDecoration(
//                                         border: new Border.all(
//                                             width: 1.0, color: Colors.black),
//                                         color: Colors.grey),
//                                     height: MediaQuery.of(context).size.height *
//                                         0.14,
//                                     width: MediaQuery.of(context).size.width *
//                                         0.45,
//                                     child: Stack(children: <Widget>[
//                                       Center(
//                                         child: new Icon(
//                                           Icons.play_circle_outline,
//                                           color: Colors.black,
//                                           size: 30,
//                                         ),
//                                       ),
//                                       Positioned(
//                                         bottom:
//                                             MediaQuery.of(context).size.height *
//                                                 0.01,
//                                         right:
//                                             MediaQuery.of(context).size.width *
//                                                 0.03,
//                                         child: Text(
//                                           snapShot.data[index]['duration'] !=
//                                                   null
//                                               ? snapShot.data[index]['duration']
//                                               : '00:00:00',
//                                           style: TextStyle(color: Colors.white),
//                                         ),
//                                       )
//                                     ]),
//                                   ),
//                                   Container(
//                                       height:
//                                           MediaQuery.of(context).size.height *
//                                               0.14,
//                                       child: Padding(
//                                         padding:
//                                             const EdgeInsets.only(left: 10.0),
//                                         child: Column(
//                                             mainAxisAlignment:
//                                                 MainAxisAlignment.spaceEvenly,
//                                             children: <Widget>[
//                                               Text(
//                                                   snapShot.data[index]
//                                                               ['videoTitle'] !=
//                                                           null
//                                                       ? snapShot.data[index]
//                                                           ['videoTitle']
//                                                       : 'Liked Video',
//                                                   style:
//                                                       TextStyle(fontSize: 16.0),
//                                                   textAlign: TextAlign.start),
//                                               Text(
//                                                 DateUtil().formattedDate(
//                                                     DateTime.parse(
//                                                         snapShot.data[index]
//                                                             ['createdAt'])),
//                                                 style:
//                                                     TextStyle(fontSize: 16.0),
//                                               ),
//                                               Text(
//                                                 "100 Likes",
//                                                 style:
//                                                     TextStyle(fontSize: 16.0),
//                                               )
//                                             ]),
//                                       ))
//                                 ],
//                               ),
//                             ),
//                           ),
//                           onTap: () {
//                             Navigator.push(
//                                 context,
//                                 MaterialPageRoute(
//                                     builder: (context) => SingleVideoPlayer(
//                                         snapShot.data[index]
//                                             [Constants.OBJECT_URL])));
//                           },
//                         );
//                       },
//                     );
//             }));
//   }
// }

// class FavouriteView extends StatefulWidget {
//   @override
//   State<StatefulWidget> createState() {
//     return favourite();
//   }
// }

// class favourite extends State<FavouriteView> {
//   HistoryBloc historyBloc = HistoryBloc();
//   ApiProvider apiProvider = ApiProvider();
//   var history;
//   List myList;
//   ScrollController _scrollController = ScrollController();
//   int _currentMax = 10;
//   int currentindex = 1;
//   @override
//   void initState() {
//     history = apiProvider.gethistoryVideos(ApplicationConstants.userId);
//     // TODO: implement initState
//     super.initState();
//     _scrollController.addListener(() {
//       if (_scrollController.position.pixels ==
//           _scrollController.position.maxScrollExtent) {
//         _getMoreData();
//       }
//     });
//   }

//   _getMoreData() {
//     logger.w('----pagination------$currentindex');
//     // upload = apiProvider.getUploadVideos(ApplicationConstants.userId,currentIndex+1);
//     _currentMax = _currentMax + 10;
//     setState(() {});
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         body: FutureBuilder(
//             future: history,
//             builder: (context, snapShot) {
//               print('-----3333----${snapShot.data}');
//               return snapShot.connectionState == ConnectionState.waiting
//                   ? Center(
//                       child: CircularProgressIndicator(),
//                     )
//                   : ListView.builder(
//                       shrinkWrap: true,
//                       itemCount: snapShot.data.length,
//                       scrollDirection: Axis.vertical,
//                       itemBuilder: (BuildContext context, int index) {
//                         return GestureDetector(
//                           child: Card(
//                             //padding: const EdgeInsets.all(7.0),
//                             child: Container(
//                               // decoration: new BoxDecoration(
//                               //     border: new Border.all(width: 0.5, color: Colors.white)),
//                               child: Row(
//                                 children: <Widget>[
//                                   Container(
//                                     decoration: new BoxDecoration(
//                                         border: new Border.all(
//                                             width: 1.0, color: Colors.black),
//                                         color: Colors.grey),
//                                     height: MediaQuery.of(context).size.height *
//                                         0.14,
//                                     width: MediaQuery.of(context).size.width *
//                                         0.45,
//                                     child: Stack(children: <Widget>[
//                                       Center(
//                                         child: new Icon(
//                                           Icons.play_circle_outline,
//                                           color: Colors.black,
//                                           size: 30,
//                                         ),
//                                       ),
//                                       Positioned(
//                                         bottom:
//                                             MediaQuery.of(context).size.height *
//                                                 0.01,
//                                         right:
//                                             MediaQuery.of(context).size.width *
//                                                 0.03,
//                                         child: Text(
//                                           snapShot.data[index]['duration'] !=
//                                                   null
//                                               ? snapShot.data[index]['duration']
//                                               : '00:00:00',
//                                           style: TextStyle(color: Colors.white),
//                                         ),
//                                       )
//                                     ]),
//                                   ),
//                                   Container(
//                                       height:
//                                           MediaQuery.of(context).size.height *
//                                               0.14,
//                                       child: Padding(
//                                         padding:
//                                             const EdgeInsets.only(left: 10.0),
//                                         child: Column(
//                                             mainAxisAlignment:
//                                                 MainAxisAlignment.spaceEvenly,
//                                             children: <Widget>[
//                                               Text(
//                                                   snapShot.data[index]
//                                                               ['videoTitle'] !=
//                                                           null
//                                                       ? snapShot.data[index]
//                                                           ['videoTitle']
//                                                       : 'Upload Video',
//                                                   style:
//                                                       TextStyle(fontSize: 16.0),
//                                                   textAlign: TextAlign.start),
//                                               Text(
//                                                 DateUtil().formattedDate(
//                                                     DateTime.parse(
//                                                         snapShot.data[index]
//                                                             ['createdAt'])),
//                                                 style:
//                                                     TextStyle(fontSize: 16.0),
//                                               ),
//                                               Text(
//                                                 "100 Likes",
//                                                 style:
//                                                     TextStyle(fontSize: 16.0),
//                                               )
//                                             ]),
//                                       ))
//                                 ],
//                               ),
//                             ),
//                           ),
//                           onTap: () {
//                             Navigator.push(
//                                 context,
//                                 MaterialPageRoute(
//                                     builder: (context) => SingleVideoPlayer(
//                                         snapShot.data[index]
//                                             [Constants.OBJECT_URL])));
//                           },
//                         );
//                       },
//                     );
//             }));
//   }
// }

class ImageLoader extends StatefulWidget {
  String url;

  ImageLoader({this.url});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ImageLoader();
  }
}

class _ImageLoader extends State<ImageLoader> {
  bool isClicked = false;
  HistoryBloc historyBloc = HistoryBloc();
  GlobalKey<ScaffoldState> scaffoldkey = GlobalKey<ScaffoldState>();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (isClicked) {
      historyBloc.playVideo(context, widget.url);
    }
    // TODO: implement build
    return Scaffold(
        key: scaffoldkey,
        body: GestureDetector(
          onTap: () {
            if (isClicked) {
              isClicked = false;
            } else {
              isClicked = true;
            }
            setState(() {});
          },
          child: !isClicked
              ? Container(
                  color: Colors.black,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(
                            Icons.play_arrow,
                            size: 30,
                            color: Colors.white,
                          )
                        ],
                      )
                    ],
                  ))
              : Center(
                  child: CircularProgressIndicator(),
                ),
        ));
  }
}

abstract class TabBarEvents {}

class LoadMore extends TabBarEvents {}

class ItemClicked extends TabBarEvents {}

class TabBarCommonBloc {
  int tabIndex = 0;
  BuildContext context;
  GlobalKey<ScaffoldState> scaffold;
  var response;
  String snackText = "Loading";
  bool isLoading = false;
  ApiProvider apiProvider = ApiProvider();
  final uploadStreamcontroller = StreamController<TabBarEvents>();
  Sink<TabBarEvents> get uploadStreamSink => uploadStreamcontroller.sink;

  final uploadDataStreamCon = StreamController();
  StreamSink get uploadDataSink => uploadDataStreamCon.sink;
  Stream get uploadDataStream => uploadDataStreamCon.stream;

  initState(context) {
    this.context = context;
    uploadStreamcontroller.stream.listen(onData);
  }

  showAlert({text}) {
    print(">>>>>>>---SNACKBAR---$text");
    // if(isLoading){
    //   snackText = "please wait";
    // }else{
    //   snackText = "Loading";
    // }
    scaffold.currentState.removeCurrentSnackBar();
    scaffold.currentState.showSnackBar(SnackBar(
        behavior: SnackBarBehavior.floating,
        content: Text(text),
        duration: Duration(milliseconds: 100)));
  }

  getCurrentScreenInitialData(int index) {
    if (index == 0) {
      if (StaticNeeds.uploadScreendata.length == 0) {
        //uploadDataSink.add(StaticNeeds.uploadScreendata);
        getUploads();
      } else {
        uploadDataSink.add(StaticNeeds.uploadScreendata);
      }
    } else if (index == 1) {
      if (StaticNeeds.likeScreendata.length == 0) {
        //uploadDataSink.add(StaticNeeds.likeScreendata);
        getLikedData();
      } else {
        uploadDataSink.add(StaticNeeds.likeScreendata);
      }
    } else {
      if (StaticNeeds.historyScreendata.length == 0) {
        //uploadDataSink.add(StaticNeeds.historyScreendata);
        getHistroyData();
      } else {
        uploadDataSink.add(StaticNeeds.historyScreendata);
      }
    }
  }

  dispose() {
    uploadStreamcontroller.close();
    uploadDataStreamCon.close();
  }

  onData(TabBarEvents events) async {
    if (events is LoadMore) {
      if (!isLoading) {
        if (tabIndex == 0) {
          await getUploads();
        } else if (tabIndex == 1) {
          await getLikedData();
        } else {
          await getHistroyData();
        }
      } else {
        showAlert(text: "please wait");
      }
    }
  }

  getLikedData() async {
    if (!StaticNeeds.isLikedScreenDatafetched) {
      print(">>>>>>-------LikedDta");
      isLoading = true;
      response = await apiProvider.getLikedVideos(
          ApplicationConstants.userId, StaticNeeds.likeScreenPageNo);
      if (response.length == 0) {
        StaticNeeds.isLikedScreenDatafetched = true;
      }
      StaticNeeds.likeScreenPageNo++;
      StaticNeeds.likeScreendata.addAll(response);
      uploadDataSink.add(StaticNeeds.likeScreendata);
      isLoading = false;
    } else {
      uploadDataSink.add(StaticNeeds.likeScreendata);
      snackText = "All Data fetched";
      showAlert(text: snackText);
    }
  }

  getHistroyData() async {
    if (!StaticNeeds.isHistoryScreenDatafetched) {
      isLoading = true;
      response = await apiProvider.gethistoryVideos(
          ApplicationConstants.userId, StaticNeeds.histroyScreenPageNo);
      if (response.length == 0) {
        StaticNeeds.isHistoryScreenDatafetched = true;
      }
      StaticNeeds.histroyScreenPageNo++;
      StaticNeeds.historyScreendata.addAll(response);
      uploadDataSink.add(StaticNeeds.historyScreendata);
      isLoading = false;
    } else {
      uploadDataSink.add(StaticNeeds.historyScreendata);
      snackText = "All Data fetched";
      showAlert(text: snackText);
    }
  }

  getUploads() async {
    if (!StaticNeeds.isUploadScreenDatafetched) {
      isLoading = true;
      response = await apiProvider.getUploadVideos(
          ApplicationConstants.userId, StaticNeeds.uploadScreenPageNo);
      if (response.length == 0) {
        StaticNeeds.isUploadScreenDatafetched = true;
      }
      print(">>>>>>>>>>>>>>>>>------iamcominghere");
      StaticNeeds.uploadScreenPageNo++;
      StaticNeeds.uploadScreendata.addAll(response);
      print(">>>>>>>>>>>>>>>>>------iamcominghere");
      uploadDataSink.add(StaticNeeds.uploadScreendata);
      isLoading = false;
    } else {
      uploadDataSink.add(StaticNeeds.uploadScreendata);
      snackText = "All Data fetched";
      showAlert(text: snackText);
    }
  }
}

class StaticNeeds {
  static int histroyScreenPageNo = 1;
  static int uploadScreenPageNo = 1;
  static int likeScreenPageNo = 1;
  static var historyScreendata = [];
  static var uploadScreendata = [];
  static var likeScreendata = [];
  static bool isUploadScreenDatafetched = false;
  static bool isLikedScreenDatafetched = false;
  static bool isHistoryScreenDatafetched = false;
  static void disposed() {
    histroyScreenPageNo = 1;
    uploadScreenPageNo = 1;
    likeScreenPageNo = 1;
    historyScreendata = [];
    uploadScreendata = [];
    likeScreendata = [];
    isUploadScreenDatafetched = false;
    isLikedScreenDatafetched = false;
    isHistoryScreenDatafetched = false;
  }
}
