import 'dart:async';
import 'package:animated_widgets/widgets/translation_animated.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:xposure/blocs/feedBloc.dart';
import 'package:xposure/CameraWidget.dart';
import 'package:xposure/ui/historyScreen.dart';
import 'package:xposure/ui/videoDescribtionPage.dart';
import 'package:screen/screen.dart';
import 'package:video_player/video_player.dart';
import 'package:xposure/constants/strings.dart' as Constants;
import 'package:xposure/constants/images.dart' as IMAGES;
import 'package:logger/logger.dart';

class FeedScreen extends StatefulWidget {
  int index = 0;
  int pageNo = 1;
  var initialData = null;
  VideoPlayerController videoPlayerController;
  FeedScreen({this.initialData, this.videoPlayerController});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _FeedScreenState();
  }
}

class _FeedScreenState extends State<FeedScreen>
    with WidgetsBindingObserver, TickerProviderStateMixin {
  bool firstPlayer = false;
  bool secondPlayer = false;
  bool isThisIsNotPresent = false;
  FeedBloc feedBloc;
  bool isFav = false;
  bool plusClicked = false;
  StreamController _streamController = StreamController();
  VideoPlayerController _playerController1;
  VideoPlayerController _playerController2;
  Future<void> _initializeFirstPlayerFuture;
  Future<void> _initializeSecondPlayerFuture;
  String userId = "";
  final scaffoldkey = GlobalKey<ScaffoldState>();
  var snap;
  bool initialplayer = false;
  final logger = Logger();
  int ind;
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    switch (state) {
      case AppLifecycleState.resumed:
        break;
      case AppLifecycleState.inactive:
        // TODO: Handle this case.
        break;
      case AppLifecycleState.paused:
        // TODO: Handle this case.
        break;
      case AppLifecycleState.detached:
        // TODO: Handle this case.
        break;
    }
    if (state == AppLifecycleState.resumed) {
      Screen.keepOn(true);
    }
  }

//initialstate
  @override
  void initState() {
    super.initState();
    ind = 0;
    WidgetsBinding.instance.addObserver(this);
    feedBloc = FeedBloc();
    feedBloc.initState();
    if (widget.initialData != null) {
      if (!widget.videoPlayerController.value.initialized) {
        widget.videoPlayerController.initialize()
          ..then((_) {
            initialplayer = true;
            widget.videoPlayerController.play();
          });
      } else {
        initialplayer = true;
        widget.videoPlayerController.play();
      }
      feedBloc.feedList = widget.initialData;
    } else {
      firstPlayer = true;
      feedBloc.getFeedVideoList(context, widget.pageNo);
    }

    //feedBloc.getFeedVideoList(context, widget.pageNo);
  }

  Future<void> refreshPage() async {
    firstPlayer = true;
    secondPlayer = false;
    feedBloc.feedVideoController.sink.add("");
    feedBloc.getFeedVideoList(context, widget.pageNo);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    Screen.keepOn(true);
    return Scaffold(
      backgroundColor: Colors.black,
      key: scaffoldkey,
      body: Stack(
        children: <Widget>[
          new Offstage(
            offstage: ind != 0,
            child: new TickerMode(
              enabled: ind == 0,
              child: RefreshIndicator(
                onRefresh: refreshPage,
                child: StreamBuilder(
                    initialData: widget.initialData,
                    stream: feedBloc.feedVideoController.stream,
                    builder: (context, snapshot) {
                      if (snapshot.data != null) {
                        snap = snapshot.data;
                        isFav = snapshot.data[widget.index]
                            [Constants.FAVOURITE_STATUS];
                      }
                      SystemChrome.setEnabledSystemUIOverlays([]);
                      logger.d('---->>>>>${snapshot.data}');
                      return snapshot.data != null
                          ? GestureDetector(
                              onHorizontalDragEnd: (DragEndDetails details) {
                                setState(() {
                                  plusClicked = false;
                                });
                                if (details.velocity.pixelsPerSecond.dx > 0) {
                                  if (widget.videoPlayerController != null) {
                                    if (widget.index == 0) {
                                      widget.videoPlayerController.pause();
                                      firstPlayer = false;
                                      secondPlayer = true;
                                      initialplayer = false;
                                    }
                                  }
                                  _playerController1.pause();
                                  _playerController2.pause();
                                  //widget.index++;
                                  isFav = snapshot.data != null &&
                                      snapshot.data[widget.index]
                                          [Constants.FAVOURITE_STATUS];
                                  feedBloc.liked(
                                      context,
                                      scaffoldkey,
                                      snapshot.data[widget.index],
                                      widget.index);
                                  widget.index++;
                                  findLink();
                                } else if (details.velocity.pixelsPerSecond.dx <
                                    0) {
                                  if (widget.videoPlayerController != null) {
                                    if (widget.index == 0) {
                                      widget.videoPlayerController.pause();
                                      firstPlayer = false;
                                      secondPlayer = true;
                                      initialplayer = false;
                                    }
                                  }
                                  _playerController1.pause();
                                  _playerController2.pause();
                                  //widget.index++;
                                  isFav = snapshot.data != null &&
                                      snapshot.data[widget.index]
                                          [Constants.FAVOURITE_STATUS];
                                  feedBloc.disliked(
                                      context,
                                      scaffoldkey,
                                      snapshot.data[widget.index],
                                      widget.index);
                                  widget.index++;
                                  findLink();
                                }
                              },
                              child: Stack(
                                children: <Widget>[
                                  Stack(fit: StackFit.expand, children: <
                                      Widget>[
                                    Visibility(
                                      visible: initialplayer,
                                      child: widget.videoPlayerController.value
                                                  .aspectRatio >
                                              0.0
                                          ? feedBloc.feedList[widget.index]
                                                      [Constants.WIDTH] >
                                                  feedBloc.feedList[widget
                                                      .index][Constants.HEIGHT]
                                              ? Container(
                                                  height: MediaQuery.of(context)
                                                      .size
                                                      .height,
                                                  width: MediaQuery.of(context)
                                                      .size
                                                      .width,
                                                  color: Colors.black,
                                                  child: Center(
                                                    child: AspectRatio(
                                                      aspectRatio: widget
                                                          .videoPlayerController
                                                          .value
                                                          .aspectRatio,
                                                      child: VideoPlayer(widget
                                                          .videoPlayerController),
                                                    ),
                                                  ),
                                                )
                                              : SizedBox.expand(
                                                  child: FittedBox(
                                                    fit: BoxFit.cover,
                                                    child: SizedBox(
                                                      width: widget
                                                              .videoPlayerController
                                                              .value
                                                              .size
                                                              ?.width ??
                                                          0,
                                                      //height: widget.videoPlayerController.value.size?.height ?? 0,
                                                      child: widget
                                                                  .videoPlayerController !=
                                                              null
                                                          ? AspectRatio(
                                                              aspectRatio: widget
                                                                  .videoPlayerController
                                                                  .value
                                                                  .aspectRatio,
                                                              child: VideoPlayer(
                                                                  widget
                                                                      .videoPlayerController))
                                                          : Container(),
                                                    ),
                                                  ),
                                                )
                                          : Container(
                                              height: MediaQuery.of(context)
                                                  .size
                                                  .height,
                                              width: MediaQuery.of(context)
                                                  .size
                                                  .width,
                                              color: Colors.black,
                                              child: Center(
                                                child: Text(
                                                  Constants.VIDEO_ERROR,
                                                  style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 20),
                                                ),
                                              ),
                                            ),
                                    ),
                                    Visibility(
                                      visible: firstPlayer,
                                      child: Container(
                                        child: FutureBuilder<FutureBuilder>(
                                            future: _firstPlayerWidget(context),
                                            builder: (BuildContext context,
                                                AsyncSnapshot<Widget>
                                                    snapshot) {
                                              if (snapshot.hasData)
                                                return snapshot.data;

                                              return Center(
                                                  child:
                                                      CircularProgressIndicator());
                                            }),
                                      ),
                                    ),
                                    Visibility(
                                      visible: secondPlayer,
                                      child: Container(
                                        child: FutureBuilder<FutureBuilder>(
                                            future:
                                                _secondPlayerWidget(context),
                                            builder: (BuildContext context,
                                                AsyncSnapshot<Widget>
                                                    snapshot) {
                                              if (snapshot.hasData)
                                                return snapshot.data;

                                              return Center(
                                                  child:
                                                      CircularProgressIndicator());
                                            }),
                                      ),
                                    ),
                                  ]),
                                ],
                              ),
                            )
                          : Center(
                              child: CircularProgressIndicator(),
                            );
                    }),
              ),
            ),
          ),
          Positioned(
              bottom: MediaQuery.of(context).size.width * 0.15,
              left: MediaQuery.of(context).size.width * 0.2,
              child: TranslationAnimatedWidget(
                curve: Curves.easeIn,
                enabled: plusClicked,
                duration: Duration(milliseconds: 200),
                values: [
                  Offset(0, 200),
                  Offset(0, -50),
                  Offset(0, 0),
                ],
                child: Row(
                  children: <Widget>[
                    IconButton(
                      onPressed: () {
                        plusClicked = false;
                        movingToNext();
                        Navigator.of(context)
                            .push(MaterialPageRoute(
                                builder: (context) => VideoDescribtionPage(
                                    screen: Constants.FILE_PICKER)))
                            .then((_) {
                          playFromStart();
                        });
                      },
                      iconSize: MediaQuery.of(context).size.width * 0.16,
                      icon: new Image.asset(IMAGES.UPLOAD),
                    ),
                    SizedBox(
                      width: MediaQuery.of(context).size.width * 0.16,
                    ),
                    IconButton(
                      onPressed: () async {
                        movingToNext();
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => VideoRecorderExample(),
                            )).then((_) {
                          playFromStart();
                        });
                      },
                      iconSize: MediaQuery.of(context).size.width * 0.16,
                      icon: new Image.asset(IMAGES.RECORD),
                    ),
                  ],
                ),
              )),
          new Offstage(
            offstage: ind != 3,
            child: new TickerMode(
              enabled: ind == 3,
              child: Stack(
                children: <Widget>[
                  ind == 3 ? HistoryVideo() : Container(),
                  //new MaterialApp(home: new HistoryVideo(),debugShowCheckedModeBanner: false),
                  Positioned(
                      bottom: MediaQuery.of(context).size.width * 0.15,
                      left: MediaQuery.of(context).size.width * 0.2,
                      child: TranslationAnimatedWidget(
                        curve: Curves.easeIn,
                        enabled: plusClicked,
                        duration: Duration(milliseconds: 200),
                        values: [
                          Offset(0, 200),
                          Offset(0, -50),
                          Offset(0, 0),
                        ],
                        child: Row(
                          children: <Widget>[
                            IconButton(
                              onPressed: () {
                                plusClicked = false;
                                movingToNext();
                                Navigator.of(context).push(MaterialPageRoute(
                                    builder: (context) => VideoDescribtionPage(
                                        screen: Constants.FILE_PICKER)));
                              },
                              iconSize:
                                  MediaQuery.of(context).size.width * 0.16,
                              icon: new Image.asset(IMAGES.UPLOAD),
                            ),
                            SizedBox(
                              width: MediaQuery.of(context).size.width * 0.16,
                            ),
                            IconButton(
                              onPressed: () async {
                                movingToNext();
                                plusClicked = false;
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) =>
                                          VideoRecorderExample(),
                                    ));
                              },
                              iconSize:
                                  MediaQuery.of(context).size.width * 0.16,
                              icon: new Image.asset(IMAGES.RECORD),
                            ),
                          ],
                        ),
                      )),
                ],
              ),
            ),
          ),
        ],
      ),
      floatingActionButton: Container(
          height: MediaQuery.of(context).size.height * 0.13,
          width: MediaQuery.of(context).size.height * 0.13,
          child: FloatingActionButton(
              elevation: 0.0,
              backgroundColor: Color(0xff383434),
              onPressed: () async {
                // if (ind == 0) {
                chooseOption();
                // }
              },
              child: IconButton(
                iconSize: 100,
                color: Color(0xff383434),
                icon: plusClicked == true
                    ? new Image.asset(
                        IMAGES.CLOSEPLUS,
                      )
                    : new Image.asset(
                        IMAGES.PLUS,
                      ),
              ),
              isExtended: true,
              materialTapTargetSize: MaterialTapTargetSize.shrinkWrap)),
      bottomNavigationBar: SizedBox(
          // height: MediaQuery.of(context).size.height * 0.11,
          child: Theme(
              data: Theme.of(context).copyWith(canvasColor: Color(0xff383434)),
              child: BottomNavigationBar(
                type: BottomNavigationBarType.fixed,
                currentIndex: ind,
                onTap: (int index) {
                  if ((index == 3) || (index == 0)) {
                    //dont remove this method
                    StaticNeeds.disposed();
                    setState(() {
                      this.ind = index;
                      plusClicked = false;
                    });
                  }
                  if (ind == 3) {
                    movingToNext();
                  } else if (ind == 0) {
                    playFromStart();
                  }
                  logger.d('---bottom index----$ind');
                },
                items: [
                  new BottomNavigationBarItem(
                      icon: IconButton(
                        icon: Icon(
                          Icons.home,
                          color: Colors.white,
                          size: MediaQuery.of(context).size.width * 0.10,
                        ),
                      ),
                      title: new Text(
                        '  ' + Constants.HOME,
                        style: new TextStyle(
                            fontSize:
                                12 * MediaQuery.textScaleFactorOf(context),
                            color: Colors.white),
                      )),
                  new BottomNavigationBarItem(
                      icon: new IconButton(
                        icon: Icon(
                          Icons.star,
                          color: Colors.white,
                          size: MediaQuery.of(context).size.width * 0.10,
                        ),
                        onPressed: () {},
                      ),
                      title: new Text(
                        '  ' + Constants.RATE,
                        style: new TextStyle(
                            fontSize:
                                13 * MediaQuery.textScaleFactorOf(context),
                            color: Colors.white),
                      )),
                  new BottomNavigationBarItem(
                      icon: new SizedBox(
                        child: Container(),
                      ),
                      title: new Text(
                        "",
                        style: new TextStyle(fontSize: 0, color: Colors.white),
                      )),
                  new BottomNavigationBarItem(
                      icon: new IconButton(
                        icon: Icon(
                          Icons.person_outline,
                          color: Colors.white,
                          size: MediaQuery.of(context).size.width * 0.10,
                        ),
                      ),
                      title: new Text(
                        ' ' + Constants.PROFILE,
                        style: new TextStyle(
                            fontSize: 13, color: Colors.white, wordSpacing: 3),
                      )),
                  new BottomNavigationBarItem(
                      icon: StreamBuilder(
                          initialData: false,
                          stream: _streamController.stream,
                          builder: (context, snapsho) {
                            return SizedBox(
                              height: MediaQuery.of(context).size.height * 0.07,
                              child: IconButton(
                                  icon: isFav == true
                                      ? new Image.asset(
                                          IMAGES.HEART,
                                        )
                                      : new Image.asset(
                                          IMAGES.PASSION,
                                        ),
                                  onPressed: () {
                                    if (ind != 3) {
                                      if (isFav) {
                                        isFav = false;
                                      } else {
                                        isFav = true;
                                      }
                                      snap[widget.index]
                                          [Constants.FAVOURITE_STATUS] = isFav;
                                      feedBloc.madefavourite(context,
                                          snap[widget.index], widget.index);
                                      _streamController.sink.add(isFav);
                                    }
                                  }),
                            );
                          }),
                      title: new Text(
                        Constants.FAVOURITE,
                        style: new TextStyle(fontSize: 13, color: Colors.white),
                      )),
                ],
              ))),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }

//when the plus icon clicked
  chooseOption() {
    setState(() {
      plusClicked = !plusClicked;
    });
  }

//when navigate to next screen pause the video and reset the  duration
  movingToNext() async {
    isThisIsNotPresent = true;
    if (initialplayer) {
      await widget.videoPlayerController.pause();
      widget.videoPlayerController.setVolume(0.0);
      widget.videoPlayerController.seekTo(Duration(seconds: 0));
    }
    if (firstPlayer) {
      await _playerController1.pause();
      _playerController1.setVolume(0.0);
      _playerController1.seekTo(Duration(seconds: 0));
    }
    if (secondPlayer) {
      await _playerController2.pause();
      _playerController2.setVolume(0.0);
      _playerController2.seekTo(Duration(seconds: 0));
    }
  }

//return to feed screen initialize the videoplayer
  playFromStart() {
    plusClicked = false;
    isThisIsNotPresent = false;
    if (firstPlayer) {
      _playerController1.setVolume(1.0);
    }
    if (secondPlayer) {
      _playerController2.setVolume(1.0);
    }
    if (initialplayer) {
      widget.videoPlayerController.setVolume(1.0);
      widget.videoPlayerController.play();
    }
  }

//when swiping the page find the index of the video url and initialize the video player accordingly
  void findLink() {
    setState(() {
      if (widget.index == feedBloc.feedList.length - 1) {
        _playerController1.pause();
        _playerController2.pause();
        _initFirstController(feedBloc.feedList[0][Constants.OBJECT_URL]);
        widget.index = 0;
        firstPlayer = true;
        secondPlayer = false;
      } else {
        if (widget.index % 2 == 0) {
          firstPlayer = true;
          secondPlayer = false;
        } else {
          secondPlayer = true;
          firstPlayer = false;
        }
        getPageData();
      }
    });
  }

//pagination api call
  Future<void> getPageData() async {
    if (widget.index >= feedBloc.feedList.length / 3) {
      widget.pageNo = widget.pageNo + 1;
      await feedBloc.getFeedVideoList(context, widget.pageNo);
    }
  }

  //FirstPlayerInitialize
  void _initFirstController(String link) {
    _playerController1 = VideoPlayerController.network(link);

    _initializeFirstPlayerFuture = _playerController1.initialize().then((_) {
      setState(() {
        _playerController1.setLooping(true);
      });
    });
  }

  //StartFirstPlayer
  Future<void> _startFirstPlayer() async {
    if (_playerController1 == null) {
      // If there was no controller, just create a new one
      _initFirstController(
          feedBloc.feedList[widget.index][Constants.OBJECT_URL]);
      _playerController1.play();
    } else {
      if (firstPlayer) {
        _playerController1.play();
      } else {
        //_firstController.pause();
        // If there was a controller, we need to dispose of the old one first
        final oldController = _playerController1;

        // Registering a callback for the end of next frame
        // to dispose of an old controller
        // (which won't be used anymore after calling setState)
        WidgetsBinding.instance.addPostFrameCallback((_) async {
          await oldController.dispose();

          // Initing new controller
          _initFirstController(
              feedBloc.feedList[widget.index + 1][Constants.OBJECT_URL]);
          _playerController1.pause();
        });
      }
    }
  }

  //SecondPlayerInitialize
  void _initSecondController(String link) {
    _playerController2 = VideoPlayerController.network(link);
    _initializeSecondPlayerFuture = _playerController2.initialize().then((_) {
      setState(() {
        _playerController2.setLooping(true);
      });
    });
  }

  //StartSecondPlayer
  Future<void> _startSecondPlayer() async {
    if (_playerController2 == null) {
      // If there was no controller, just create a new one
      _initSecondController(
          feedBloc.feedList[widget.index + 1][Constants.OBJECT_URL]);
      _playerController2.pause();
    } else {
      if (secondPlayer) {
        _playerController2.play();
      } else {
        //_secondController.pause();
        // If there was a controller, we need to dispose of the old one first
        final oldController1 = _playerController2;

        // Registering a callback for the end of next frame
        // to dispose of an old controller
        // (which won't be used anymore after calling setState)
        WidgetsBinding.instance.addPostFrameCallback((_) async {
          await oldController1.dispose();

          // Initing new controller
          _initSecondController(
              feedBloc.feedList[widget.index + 1][Constants.OBJECT_URL]);
          _playerController2.pause();
        });
      }
    }
  }

//first video player
  Future<FutureBuilder> _firstPlayerWidget(BuildContext context) async {
    await _startFirstPlayer();
    return FutureBuilder(
        future: _initializeFirstPlayerFuture,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (_playerController1.value.aspectRatio > 0.0) {
              if (feedBloc.feedList[widget.index][Constants.WIDTH] >
                  feedBloc.feedList[widget.index][Constants.HEIGHT]) {
                return Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  color: Colors.black,
                  child: Center(
                    child: AspectRatio(
                      aspectRatio: _playerController1.value.aspectRatio,
                      child: VideoPlayer(_playerController1),
                    ),
                  ),
                );
              } else {
                return SizedBox.expand(
                    child: FittedBox(
                  fit: BoxFit.cover,
                  child: SizedBox(
                    width: _playerController1.value.size?.width ?? 0,
                    //height: _playerController1.value.size?.height ?? 0,
                    child: AspectRatio(
                      aspectRatio: _playerController1.value.aspectRatio,
                      child: VideoPlayer(_playerController1),
                    ),
                  ),
                ));
              }
            } else {
              return Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                color: Colors.black,
                child: Center(
                  child: Text(
                    Constants.VIDEO_ERROR,
                    style: TextStyle(color: Colors.white, fontSize: 20),
                  ),
                ),
              );
            }
          } else {
            return Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                color: Colors.black,
                child: Center(child: CircularProgressIndicator()));
          }
        });
  }

//second videoplayer
  Future<FutureBuilder> _secondPlayerWidget(BuildContext context) async {
    await _startSecondPlayer();

    return FutureBuilder(
        future: _initializeSecondPlayerFuture,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (_playerController2.value.aspectRatio > 0.0) {
              if (feedBloc.feedList[widget.index][Constants.WIDTH] >
                  feedBloc.feedList[widget.index][Constants.HEIGHT]) {
                return Container(
                  height: MediaQuery.of(context).size.height,
                  width: MediaQuery.of(context).size.width,
                  color: Colors.black,
                  child: Center(
                    child: AspectRatio(
                      aspectRatio: _playerController2.value.aspectRatio,
                      child: VideoPlayer(_playerController2),
                    ),
                  ),
                );
              } else {
                return SizedBox.expand(
                    child: FittedBox(
                  fit: BoxFit.cover,
                  child: SizedBox(
                    width: _playerController2.value.size?.width ?? 0,
                    //height: _playerController2.value.size?.height ?? 0,
                    child: AspectRatio(
                      aspectRatio: _playerController2.value.aspectRatio,
                      child: VideoPlayer(_playerController2),
                    ),
                  ),
                ));
              }
            } else {
              return Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                color: Colors.black,
                child: Center(
                  child: Text(
                    Constants.VIDEO_ERROR,
                    style: TextStyle(color: Colors.white, fontSize: 20),
                  ),
                ),
              );
            }
          } else {
            return Container(
                height: MediaQuery.of(context).size.height,
                width: MediaQuery.of(context).size.width,
                color: Colors.black,
                child: Center(child: CircularProgressIndicator()));
          }
        });
  }
}
