// import 'package:flutter/material.dart';
// import 'package:xposure/blocs/feedVideoBloc.dart';
// import 'package:xposure/video_render.dart';
// import 'package:preload_page_view/preload_page_view.dart';
// import 'package:flutter/services.dart';
// import 'package:screen/screen.dart';
// import 'package:swipedetector/swipedetector.dart';

// class FeedVideoScreen extends StatefulWidget {
//   @override
//   State<StatefulWidget> createState() {
//     // TODO: implement createState
//     return _FeedVideoScreen();
//   }
// }

// class _FeedVideoScreen extends State<FeedVideoScreen>
//     with WidgetsBindingObserver {
//   FeedVideoBloc feedVideoBloc;
//   int i;
//   bool isBuilded = false;
//   final scaffoldkey = GlobalKey<ScaffoldState>();

//   @override
//   void initState() {
//     i = 0;
//     super.initState();
//     feedVideoBloc = FeedVideoBloc();
//     feedVideoBloc.initState();
//     feedVideoBloc.getFeedVideoList(context);
//     WidgetsBinding.instance.addObserver(this);
//   }

//   @override
//   void dispose() {
//     // TODO: implement dispose
//     feedVideoBloc.dispose(context);
//     super.dispose();
//   }

//   @override
//   void didChangeAppLifecycleState(AppLifecycleState state) async {
//     print(">>>>>>>>>>>>DDDDDDDDDD${state}");
//     switch (state) {
//       case AppLifecycleState.resumed:
//         print(">>>>>IAM HERE");
//         break;
//       case AppLifecycleState.inactive:
//         // TODO: Handle this case.
//         break;
//       case AppLifecycleState.paused:
//         // TODO: Handle this case.
//         break;
//       case AppLifecycleState.detached:
//         // TODO: Handle this case.
//         break;
//     }
//     if (state == AppLifecycleState.resumed) {
//       print(">>>>>>>>12345>>>>DDDDDDDDDD${state}");
//       Screen.keepOn(true);
//     }
//   }

//   Future<void> refreshPage() async {
//     feedVideoBloc.getFeedVideoList(context);
//   }

//   @override
//   Widget build(BuildContext context) {
//     i++;

//     // TODO: implement build
//     Screen.keepOn(true);
//     // print(">>>>>>>>IAMHARIHARAN$name");
//     return Scaffold(
//       key: scaffoldkey,
//       body: StreamBuilder(
//         stream: feedVideoBloc.feedVideoController.stream,
//         builder: (context, snapshot) {
//           // print(">>>>>>>>>!!!!!${snapshot.data}");

//           if (snapshot.data != null) {
//             feedVideoBloc.currentData(snapshot, 0);
//           }
//           isBuilded = true;
//           return snapshot.data != null
//               ? RefreshIndicator(
//                   onRefresh: refreshPage,
//                   child: Stack(
//                     children: <Widget>[
//                       // GestureDetector(
//                       // child:
//                       PreloadPageView.builder(
//                         controller: feedVideoBloc.preLoadpageController,
//                         itemCount: snapshot.data.length,
//                         scrollDirection: Axis.horizontal,
//                         physics: new NeverScrollableScrollPhysics(),
//                         preloadPagesCount: 10,
//                         itemBuilder: (context, position) {
//                           bool isFav =
//                               snapshot.data[position]['favouriteStatus'];
//                           // print(">>>>>>>IAMCALEED----${feedVideoBloc.isOnPageTurning}");
//                           // print(">>>>>>>feedVideoBloc.current----${feedVideoBloc.current}");

//                           return SwipeDetector(
//                             child: StreamBuilder(
//                                 stream: feedVideoBloc
//                                     .videoPausePlaycontroller.stream,
//                                 builder: (context, snapsho) {
//                                   // print(">>>>>>>>>>>ISPAUSED${snapsho.data}");
//                                   SystemChrome.setEnabledSystemUIOverlays([]);
//                                   return Stack(children: <Widget>[
//                                     Container(
//                                       color: Colors.black,
//                                       child: VideoPlayerRender(
//                                         pageIndex: position,
//                                         currentPageIndex: feedVideoBloc.current,
//                                         isPaused: snapsho.data == "goToNext"
//                                             ? true
//                                             : feedVideoBloc.isOnPageTurning,
//                                         URL: snapshot.data[position]
//                                             ['objectUrl'],
//                                       ),
//                                     ),
//                                     Positioned(
//                                       right: MediaQuery.of(context).size.width *
//                                           0.10,
//                                       bottom:
//                                           MediaQuery.of(context).size.height *
//                                               0.15,
//                                       child: StreamBuilder(
//                                           stream: feedVideoBloc
//                                               .videoStatuscontroller.stream,
//                                           builder: (context, snapsho) {
//                                             return Column(children: <Widget>[
//                                               Container(
//                                                   decoration: BoxDecoration(
//                                                       color: Colors.white38,
//                                                       shape: BoxShape.circle),
//                                                   child: IconButton(
//                                                       color: isFav == true
//                                                           ? Colors.amber
//                                                           : Colors.white,
//                                                       iconSize: 40,
//                                                       icon: new Image.asset(
//                                                         'assets/images/star.png',
//                                                         color: isFav == true
//                                                             ? Colors.amber
//                                                             : Colors.white,
//                                                       ),
//                                                       onPressed: () {
//                                                         print(">>>>>>>>>IAMCLICKING");
//                                                         if (isFav) {
//                                                           isFav = false;
//                                                         } else {
//                                                           isFav = true;
//                                                         }

//                                                         print(">>>>${snapshot}");
//                                                         feedVideoBloc
//                                                             .madefavourite(
//                                                                 context,snapshot.data[position],position);
//                                                       })),
//                                               SizedBox(
//                                                 height: MediaQuery.of(context)
//                                                         .size
//                                                         .height *
//                                                     0.05,
//                                               ),
//                                               Container(
//                                                 decoration: BoxDecoration(
//                                                     color: Colors.white38,
//                                                     shape: BoxShape.circle),
//                                                 child: IconButton(
//                                                     iconSize: 40,
//                                                     icon: new Image.asset(
//                                                       'assets/images/image4.png',
//                                                     ),
//                                                     onPressed: () {}),
//                                               ),
//                                             ]);
//                                           }),
//                                     ),
//                                   ]);
//                                 }),
//                              onSwipeRight: () {
//                                 print(
//                                     ">>>>>---${snapshot.data[position]['objectUrl']}");
//                                     print(
//                                     ">>>>>-latest--${snapshot.data[position]}");
                                    
//                                 feedVideoBloc.liked(context, scaffoldkey,snapshot.data[position],position);
//                               },
//                               onSwipeLeft: () {
//                                 feedVideoBloc.disliked(context, scaffoldkey,snapshot.data[position],position);
//                               },
//                                swipeConfiguration: SwipeConfiguration(
//                                   horizontalSwipeMaxHeightThreshold: 50.0,
//                                   horizontalSwipeMinDisplacement: 50.0,
//                                   horizontalSwipeMinVelocity: 100.0),
//                             );
//                           //   },
//                           // );

//                           //g
//                         },
//                       ),

//                       Align(
//                           alignment: Alignment.bottomCenter,
//                           child: Theme(
//                               data: Theme.of(context)
//                                   .copyWith(canvasColor: Colors.transparent),
//                               child: BottomNavigationBar(
//                                 type: BottomNavigationBarType.fixed,
//                                 items: [
//                                   new BottomNavigationBarItem(
//                                       icon: new SizedBox(
//                                         height: 60,
//                                         width: 60,
//                                         child: StreamBuilder(
//                                             builder: (context, snapshot) {
//                                           return Container(
//                                             decoration: BoxDecoration(
//                                                 color: Colors.white38,
//                                                 shape: BoxShape.circle),
//                                             child: new IconButton(
//                                                 icon: new Image.asset(
//                                                   'assets/images/image2.png',
//                                                 ),
//                                                 onPressed: () {}),
//                                           );
//                                         }),
//                                       ),
//                                       title: new Text(
//                                         "",
//                                         style: new TextStyle(fontSize: 0),
//                                       )),
//                                   new BottomNavigationBarItem(
//                                       icon: new SizedBox(
//                                         height: 80,
//                                         width: 80,
//                                         child: Container(
//                                           decoration: BoxDecoration(
//                                               color: Colors.white38,
//                                               shape: BoxShape.circle),
//                                           child: new IconButton(
//                                               icon: new Image.asset(
//                                                   'assets/images/plus.png'),
//                                               onPressed: () {
//                                                 feedVideoBloc
//                                                     .goToCamera(context);
//                                               }),
//                                         ),
//                                       ),
//                                       title: new Text(
//                                         "",
//                                         style: new TextStyle(fontSize: 0),
//                                       )),
//                                   new BottomNavigationBarItem(
//                                       icon: new SizedBox(
//                                         height: 60,
//                                         width: 60,
//                                         child: Container(
//                                           decoration: BoxDecoration(
//                                               color: Colors.white38,
//                                               shape: BoxShape.circle),
//                                           child: new IconButton(
//                                               icon: new Image.asset(
//                                                 'assets/images/image3.png',
//                                               ),
//                                               onPressed: () {
//                                                 feedVideoBloc
//                                                     .goToHistroyScreen(context);
//                                               }),
//                                         ),
//                                       ),
//                                       title: new Text(
//                                         "",
//                                         style: new TextStyle(fontSize: 0),
//                                       )),
//                                 ],
//                               ))),
//                     ],
//                   ))
//               : Center(
//                   child: CircularProgressIndicator(),
//                 );
//         },
//       ),
//     );
//   }
// }
