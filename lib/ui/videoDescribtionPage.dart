import 'dart:async';
import 'dart:io';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:logger/logger.dart';
import 'package:xposure/ui/customAutocompleteTextview.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:video_player/video_player.dart';
import 'package:xposure/constants/strings.dart' as Constants;
import 'package:xposure/constants/images.dart' as IMAGES;
import '../video.dart';

class VideoDescribtionPage extends StatefulWidget {
  String screen;
  String url;
  VideoPlayerController controller;
  VideoDescribtionPage({this.screen, this.controller, this.url});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _VideoDescribtion();
  }
}

class _VideoDescribtion extends State<VideoDescribtionPage> {
  static const platform = const MethodChannel(Constants.VIDEO_STREAM);
  static const tokenPlatform = const MethodChannel(Constants.USERID);
  String videoname = null;
  String describtion = null;
  var file;
  List<String> tags = List();
  String tagsString = "";
  static const refreshPlatform =
      const MethodChannel(Constants.REFRESH_TAPS_PAGE);
  TextEditingController _nameTextcontroller = TextEditingController();
  TextEditingController _textFieldController = TextEditingController();
  StreamController _streamController = StreamController.broadcast();
  VideoPlayerController controller;
  String describtionn = "";
  String videoTitle = "";
  String videoPath = "";
  String duration = "";
  String userToken;
  var snapBar;
  final logger = Logger();

  @override
  void initState() {
    videoPath = widget.url;
    getFliePath();
    successfullyUploadedToAWS();
    if (widget.screen == Constants.FILE_PICKER) {
      file = getPath();
    }
    super.initState();
  }

  static const codec = StringCodec();
  //ios upload service call
  getFliePath() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String userToken = await sharedPreferences.getString(Constants.USER_ID);
    describtionn = describtion != null ? describtion : "";
    BinaryMessages.setMessageHandler(Constants.FILE_PATH,
        (ByteData message) async {
      return codec.encodeMessage(videoPath +
          "^" +
          userToken +
          "^" +
          videoname +
          "^" +
          describtionn +
          "^" +
          tagsString +
          "^" +
          duration);
    });
  }

  successfullyUploadedToAWS() async {
    BinaryMessages.setMessageHandler(Constants.SUCCESS,
        (ByteData message) async {
      return codec.encodeMessage(videoPath);
    });
  }

  static const MethodChannel methodChannel =
      MethodChannel('samples.flutter.io/battery');
  static const EventChannel eventChannel =
      EventChannel('samples.flutter.io/charging');

  String _batteryLevel = 'Battery level: unknown.';
  String _chargingStatus = 'Battery status: unknown.';
  Future<void> _uploadVideoToIos() async {
    String batteryLevel;
    try {
      final int result = await methodChannel.invokeMethod('uploadVideoToIos');
      batteryLevel = 'Battery level: $result%.';
    } on PlatformException {
      batteryLevel = 'Failed to get battery level.';
    }
    setState(() {
      _batteryLevel = batteryLevel;
    });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _nameTextcontroller.dispose();
    _textFieldController.dispose();
    _streamController.close();
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    List<String> tag = List<String>();
    var currentFile;
    return Scaffold(
      backgroundColor: Colors.black,
      body: FutureBuilder(
        future: widget.screen == Constants.FILE_PICKER
            ? file
            : getUrlFromVideoRecoder(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done &&
              snapshot.data == null) {
            goback();
            return Container();
          } else if (snapshot.connectionState == ConnectionState.done &&
              snapshot.data != null) {
            snapBar = snapshot.data;
            return SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Container(
                      width: MediaQuery.of(context).size.width / 2,
                      height: MediaQuery.of(context).size.height / 2.5,
                      child: LocalVideoPreviewer(url: snapshot.data)),
                  Padding(
                    padding: const EdgeInsets.only(
                      right: 40,
                      left: 40,
                      top: 10,
                    ),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          child: Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                Constants.ADD_NAME_TO_YOUR_VIDEO,
                                style: TextStyle(
                                    fontFamily: 'Montserrat',
                                    letterSpacing: 1,
                                    color: Colors.white,
                                    fontSize:
                                        MediaQuery.of(context).size.height *
                                            0.025,
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                          padding: const EdgeInsets.only(bottom: 10),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width / 1.25,
                          height: MediaQuery.of(context).size.height * 0.07,
                          decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            borderRadius: BorderRadius.circular(10.0),
                            color: Colors.white,
                          ),
                          child: StreamBuilder(
                            stream: _streamController.stream,
                            initialData: null,
                            builder: (context, snap) {
                              return Padding(
                                  padding: const EdgeInsets.only(
                                      right: 20, left: 10),
                                  child: TextField(
                                      controller: _nameTextcontroller,
                                      decoration: InputDecoration(
                                          errorText: snap.data,
                                          border: InputBorder.none)));
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      right: 40,
                      left: 40,
                      top: 10,
                    ),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          child: Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                Constants.DESCRIBE_IT,
                                style: TextStyle(
                                    fontFamily: 'Montserrat',
                                    letterSpacing: 1,
                                    color: Colors.white,
                                    fontSize:
                                        MediaQuery.of(context).size.height *
                                            0.025,
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                          padding: const EdgeInsets.only(bottom: 10),
                        ),
                        Container(
                          height: MediaQuery.of(context).size.height * 0.17,
                          width: MediaQuery.of(context).size.width / 1.25,
                          decoration: BoxDecoration(
                            shape: BoxShape.rectangle,
                            borderRadius: BorderRadius.circular(10.0),
                            color: Colors.white,
                          ),
                          child: Padding(
                              padding:
                                  const EdgeInsets.only(right: 20, left: 10),
                              child: TextField(
                                controller: _textFieldController,
                                maxLines: null,
                                maxLength: null,
                                textInputAction: TextInputAction.newline,
                                keyboardType: TextInputType.multiline,
                                decoration:
                                    InputDecoration(border: InputBorder.none),
                              )),
                        )
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        right: 40, left: 40, top: 10, bottom: 10),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          child: Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                Constants.PICK_SOME_TAGS,
                                style: TextStyle(
                                    fontFamily: 'Montserrat',
                                    letterSpacing: 1,
                                    color: Colors.white,
                                    fontSize:
                                        MediaQuery.of(context).size.height *
                                            0.025,
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                          padding: const EdgeInsets.only(bottom: 10),
                        ),
                        Container(
                            width: MediaQuery.of(context).size.width / 1.25,

                            decoration: BoxDecoration(
                              shape: BoxShape.rectangle,
                              borderRadius: BorderRadius.circular(10.0),
                              color: Colors.white,
                            ),
                            child: AutoCompleteTextView(
                              tags: tag,
                            ))
                      ],
                    ),
                  ),
                  Container(
                  height: MediaQuery.of(context).size.height * 0.1,
                    child: Padding(
                        padding: const EdgeInsets.only(bottom: 2),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Column(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  IconButton(
                                    iconSize: MediaQuery.of(context).size.height *
                                        0.025,
                                    icon: Image.asset(
                                      IMAGES.REDO,
                                      height: 50,
                                      width: 50,
                                    ),
                                    onPressed: () {
                                      Navigator.pop(context);
                                    },
                                  ),
                                  Text(Constants.CANCEL,
                                      style: TextStyle(
                                          fontSize:
                                              MediaQuery.of(context).size.height *
                                                  0.025,
                                          fontFamily: 'Montserrat',
                                          fontWeight: FontWeight.bold,
                                          color: Colors.white))
                                ]),
                            Column(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                IconButton(
                                  iconSize:
                                      MediaQuery.of(context).size.height * 0.025,
                                  icon: Image.asset(IMAGES.UPLOAD,
                                      height: 50, width: 50),
                                  onPressed: () async {
                                    if (widget.screen == Constants.FILE_PICKER) {
                                      videoPath = snapshot.data;
                                      currentFile = new File(snapshot.data);
                                    } else {
                                      videoPath = widget.url;
                                      currentFile = new File(videoPath);
                                    }
                                    controller =
                                        VideoPlayerController.file(currentFile);
                                    controller.initialize().then((_) async {
                                      duration = transformMilliSeconds(controller
                                              .value.duration.inMilliseconds)
                                          .toString();
                                    });
                                    if (_nameTextcontroller.text.toString() ==
                                            null ||
                                        _nameTextcontroller.text
                                                .toString()
                                                .trim() ==
                                            "") {
                                      _streamController.sink
                                          .add(Constants.NAME_CANNOT_BE_NULL);
                                    } else {
                                      for (int i = 0; i < Tags.tags.length; i++) {
                                        if (i != Tags.tags.length - 1) {
                                          tagsString = tagsString +
                                              Tags.tags[i].toString().trim() +
                                              ",";
                                        } else {
                                          tagsString = tagsString +
                                              Tags.tags[i].toString().trim();
                                        }
                                      }
                                      videoname = _nameTextcontroller.text
                                          .toString()
                                          .trim();
                                      describtion =
                                          _textFieldController.text.toString();

                                      getFilePath(snapshot.data);
                                      Navigator.pop(context);
                                    }
                                  },
                                ),
                                Text(
                                  Constants.UPLOAD,
                                  style: TextStyle(
                                      fontSize:
                                          MediaQuery.of(context).size.height *
                                              0.025,
                                      fontFamily: 'Montserrat',
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white),
                                )
                              ],
                            )
                          ],
                        )),
                  )

                  //SizedBox(height: 300,)
                ],
              ),
            );
          } else {
            return Container(
              child: Text(""),
            );
          }
        },
      ),
      // bottomNavigationBar: BottomNavigationBar(
      //   backgroundColor: Colors.black,
      //   items: [
      //     BottomNavigationBarItem(
      //       title: Text(Constants.CANCEL,
      //           style: TextStyle(
      //               fontSize: MediaQuery.of(context).size.height * 0.025,
      //               fontFamily: 'Montserrat',
      //               fontWeight: FontWeight.bold,
      //               color: Colors.white)),
      //       icon: IconButton(
      //           iconSize: MediaQuery.of(context).size.height * 0.025,
      //           icon: Image.asset(
      //             IMAGES.REDO,
      //             height: 50,
      //             width: 50,
      //           ),
      //           onPressed: () {
      //             Navigator.pop(context);
      //           }),
      //     ),
      //     BottomNavigationBarItem(
      //         title: Text(
      //           Constants.UPLOAD,
      //           style: TextStyle(
      //               fontSize: MediaQuery.of(context).size.height * 0.025,
      //               fontFamily: 'Montserrat',
      //               fontWeight: FontWeight.bold,
      //               color: Colors.white),
      //         ),
      //         icon: IconButton(
      //           iconSize: MediaQuery.of(context).size.height * 0.025,
      //           icon: Image.asset(IMAGES.UPLOAD, height: 50, width: 50),
      //           onPressed: () async {
      //             if (widget.screen == Constants.FILE_PICKER) {
      //               videoPath = snapBar;
      //               currentFile = new File(snapBar);
      //             } else {
      //               videoPath = widget.url;
      //               currentFile = new File(videoPath);
      //             }
      //             controller = VideoPlayerController.file(currentFile);
      //             controller.initialize().then((_) async {
      //               duration = transformMilliSeconds(
      //                       controller.value.duration.inMilliseconds)
      //                   .toString();
      //             });
      //             if (_nameTextcontroller.text.toString() == null ||
      //                 _nameTextcontroller.text.toString().trim() == "") {
      //               _streamController.sink.add(Constants.NAME_CANNOT_BE_NULL);
      //             } else {
      //               for (int i = 0; i < Tags.tags.length; i++) {
      //                 if (i != Tags.tags.length - 1) {
      //                   tagsString =
      //                       tagsString + Tags.tags[i].toString().trim() + ",";
      //                 } else {
      //                   tagsString =
      //                       tagsString + Tags.tags[i].toString().trim();
      //                 }
      //               }
      //               videoname = _nameTextcontroller.text.toString().trim();
      //               describtion = _textFieldController.text.toString();

      //               getFilePath(snapBar);
      //               Navigator.pop(context);
      //             }
      //           },
      //         ))
      // ],
      // ),
    );
  }

  goback() {
    Navigator.pop(context);
  }

//getting filepath from gallery
  getPath() async {
    String filePath = await FilePicker.getFilePath(type: FileType.VIDEO);
    return filePath;
  }

  void getFilePath(filePath) async {
    try {
      if (filePath == '' || filePath == null) {
        return;
      }
      var currentFile = new File(filePath);
      VideoPlayerController controller =
          VideoPlayerController.file(currentFile);
      controller.initialize().then((_) async {
        if (controller.value.duration.inSeconds <= 180) {
          SharedPreferences sharedPreferences =
              await SharedPreferences.getInstance();
          String userId = await sharedPreferences.getString(Constants.USER_ID);
          userToken = userId;
          // stringBuffer.
          duration =
              transformMilliSeconds(controller.value.duration.inMilliseconds);
          describtionn = describtionn != null ? describtionn : "";
          final Map<String, dynamic> params = <String, dynamic>{
            'filePath': filePath,
            'userId': userId,
            'duration': duration,
            'videoName': videoname,
            'videoDescription': describtion,
            'hashTags': Tags.tags
          };

          print(">>>>>>>-------FLUTTER SIDE---DATA--->$params");

          if (Platform.isAndroid) {
            // Android-specific code
          } else if (Platform.isIOS) {
            // iOS-specific code
            _uploadVideoToIos();
          }

          //   logger.w(params);
          //   Fluttertoast.showToast(
          // msg: params.toString(),
          // toastLength: Toast.LENGTH_LONG,
          // gravity: ToastGravity.CENTER,
          // timeInSecForIos: 1,
          // backgroundColor: Colors.red,
          // textColor: Colors.white);
          await platform.invokeMethod(Constants.VIDEO_INFO, params);
        } else {
          showDialog(
            context: context,
            barrierDismissible: false,
            builder: (context) => new AlertDialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(40)),
              elevation: 20,
              title: Text(Constants.ERROR),
              content: Text(Constants.VIDEO_RESTRICTION),
              actions: <Widget>[
                FlatButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text(Constants.OK,
                        style: TextStyle(color: Colors.blue)))
              ],
            ),
          );
        }
      });
    } on Exception catch (e) {
      logger.e("Error while picking the file: " + e.toString());
    }
  }

  transformMilliSeconds(int milliseconds) {
    int hundreds = (milliseconds / 10).truncate();
    int seconds = (hundreds / 100).truncate();
    int minutes = (seconds / 60).truncate();
    int hours = (minutes / 60).truncate();

    String hoursStr = (hours % 60).toString().padLeft(2, '0');
    String minutesStr = (minutes % 60).toString().padLeft(2, '0');
    String secondsStr = (seconds % 60).toString().padLeft(2, '0');

    return "$hoursStr:$minutesStr:$secondsStr";
  }

//getting url from camera recorder
  getUrlFromVideoRecoder() async {
    return await widget.url;
  }
}

// thumbnail generator
class LocalVideoPreviewer extends StatefulWidget {
  String url;
  VideoPlayerController controller;
  LocalVideoPreviewer({this.url});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _LocalVideoPreviewer();
  }
}

class _LocalVideoPreviewer extends State<LocalVideoPreviewer> {
  VideoPlayerController videoPlayerController;
  bool initialized = false;
  String userId;
  final logger = Logger();

  @override
  void dispose() {
    videoPlayerController.dispose();
    // TODO: implement dispose
    super.dispose();
  }

  @override
  void initState() {
    var file = new File(widget.url);
    try {
      videoPlayerController = VideoPlayerController.file(file);
      videoPlayerController
        ..initialize().then((_) {
          initialized = true;
          setState(() {});
          if (videoPlayerController.value.duration.inSeconds > 180) {
            overDuration();
          }
        });
    } catch (e) {
      logger.e(">>>>>>>>>>>-----$e");
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return initialized
        ? Container(
            height: MediaQuery.of(context).size.height * 0.5,
            child: Stack(
              children: <Widget>[
                VideoPlayer(videoPlayerController),
                Positioned.fill(
                    child: Align(
                  alignment: Alignment.center,
                  child: IconButton(
                    iconSize: 50,
                    icon: Image.asset(IMAGES.PLAYBACK, height: 50, width: 50),
                    onPressed: () async {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => vidPre(url: widget.url)));
                    },
                  ),
                )),
              ],
            ))
        : Container(
            child: Center(
              child: CircularProgressIndicator(),
            ),
          );
  }
}

//videoplayer for playing thumbnail video
class vidPre extends StatefulWidget {
  String url;
  vidPre({this.url});
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _vipre();
  }
}

class _vipre extends State<vidPre> {
  VideoPlayerController _videoPlayerController;
  bool initilized = false;
  @override
  void initState() {
    File file = File(widget.url);
    _videoPlayerController = VideoPlayerController.file(file);
    _videoPlayerController.initialize()
      ..then((_) {
        initilized = true;
        _videoPlayerController.play();
        _videoPlayerController.setLooping(true);
        setState(() {});
      });
    super.initState();
  }

  @override
  void dispose() {
    _videoPlayerController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
          body: initilized
              ? Stack(
                  children: <Widget>[
                    AspectRatio(
                      aspectRatio: MediaQuery.of(context).size.width /
                          MediaQuery.of(context).size.height,
                      child: VideoPlayer(_videoPlayerController),
                    ),
                    Positioned(
                      child: Container(
                        child: IconButton(
                          iconSize: 30,
                          icon: new Image.asset(IMAGES.BACK),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                      ),
                    )
                  ],
                )
              : Center(
                  child: CircularProgressIndicator(),
                )),
      onWillPop: () {
        _videoPlayerController.pause();
        Navigator.pop(context);
      },
    );
  }
}

overDuration() {
  Fluttertoast.showToast(
      msg: Constants.VIDEO_RESTRICTION,
      toastLength: Toast.LENGTH_LONG,
      gravity: ToastGravity.CENTER,
      timeInSecForIos: 1,
      backgroundColor: Colors.red,
      textColor: Colors.white);
}
