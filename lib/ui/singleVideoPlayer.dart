import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';
import 'package:xposure/api/api.dart';
import 'package:xposure/constants/images.dart' as IMAGES;
import 'package:xposure/constants/strings.dart' as Constants;

class SingleVideoPlayer extends StatefulWidget {
  var data;
  SingleVideoPlayer(this.data);
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _SingleVideoPlayer();
  }
}

class _SingleVideoPlayer extends State<SingleVideoPlayer> {
  VideoPlayerController videoPlayerController;
  var _initialize;
  bool manualPause = false;
  @override
  void initState() {
    videoPlayerController =
        VideoPlayerController.network(widget.data[Constants.OBJECT_URL]);
    _initialize = videoPlayerController.initialize().then((_) {
      videoPlayerController.play();
      videoPlayerController.setLooping(true);
    });
    super.initState();
  }

  @override
  void dispose() {
    videoPlayerController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: GestureDetector(
      onTap: () {
        setState(() {
          // If the video is playing, pause it.
          if (videoPlayerController.value.isPlaying) {
            videoPlayerController.pause();
            manualPause = true;
          } else {
            // If the video is paused, play it.
            videoPlayerController.play();
            manualPause = false;
          }
        });
      },
      child: Stack(children: <Widget>[
        FutureBuilder(
          future: _initialize,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              if (videoPlayerController.value.aspectRatio > 0.0) {
                if (widget.data[Constants.WIDTH] > widget.data[Constants.HEIGHT]) {
                  return Container(
                      height: MediaQuery.of(context).size.height,
                      width: MediaQuery.of(context).size.width,
                      color: Colors.black,
                      child: Center(
                    child: AspectRatio(
                      aspectRatio: videoPlayerController.value.aspectRatio,
                      child: VideoPlayer(videoPlayerController),
                    ),
                  ));
                }else{
                  return SizedBox.expand(
                      child: FittedBox(
                        fit: BoxFit.cover,
                        child: SizedBox(
                          width: videoPlayerController.value.size?.width ?? 0,
                          //height: _playerController2.value.size?.height ?? 0,
                          child: Stack(children: <Widget>[
                            AspectRatio(
                              aspectRatio: videoPlayerController.value.aspectRatio,
                              child: VideoPlayer(videoPlayerController),
                            ),
                          ]),
                        ),
                      ));
                }
              }else {
                return Container(
                        height: MediaQuery.of(context).size.height,
                        width: MediaQuery.of(context).size.width,
                        color: Colors.black,
                        child: Center(
                          child: Text(
                            Constants.VIDEO_ERROR,
                            style: TextStyle(color: Colors.white, fontSize: 20),
                          ),
                        ),
                      );
              }
            } else {
              return Container(
                  color: Colors.black,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[CircularProgressIndicator()],
                      )
                    ],
                  ));
            }
          },
        ),
        Visibility(
          visible: manualPause,
          child: Positioned(
            child: Align(
              alignment: Alignment.center,
              child: IconButton(
                iconSize: 50,
                icon: Image.asset(IMAGES.PLAYBACK, height: 50, width: 50),
              ),
              //   ),
            ),
          ),
        ),
        Positioned(
          child: Container(
            child: IconButton(
              iconSize: 30,
              icon: new Image.asset(IMAGES.BACK),
              onPressed: () {
                videoPlayerController.pause();
                Navigator.pop(context);
              },
            ),
          ),
        )
      ]),
    ));
  }
}
