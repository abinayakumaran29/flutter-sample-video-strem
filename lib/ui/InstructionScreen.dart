// import 'package:flutter/material.dart';
// import 'package:xposure/blocs/instructionScreenBloc.dart';

// class InstructionScreen extends StatefulWidget {

//   @override
//   _InstructionScreen createState() => _InstructionScreen();
// }

// class _InstructionScreen extends State<InstructionScreen> {
//   InstructionBloc instructionBloc;
//   @override
//   Widget build(BuildContext context) {
//     instructionBloc = InstructionBloc();
//     return Scaffold(
//       backgroundColor: Colors.white,
//       appBar: AppBar(
//         backgroundColor: Colors.white,
//         centerTitle: true,
//         title: IconButton(
//           iconSize: 180,
//           icon: new Image.asset(
//             'assets/images/expfaces.png',
//             color: Colors.black,),
//         ),
//       ),
//       body: Stack(
//         children: <Widget>[
//           Center(child: Container(
//             child: Column(
//                   mainAxisAlignment: MainAxisAlignment.center,
//                   mainAxisSize: MainAxisSize.min,
//                   children: <Widget>[

//                     Container(
//                         child: Center(child: Column(
//                           children: <Widget>[
//                             IconButton(
//                               iconSize: 180,
//                               icon: new Image.asset(
//                                 'assets/images/swipe.png',
//                               ),
//                             ),
//                           ],
//                         ))),
//                     Container(
//                       padding: EdgeInsets.only(left: 20,right: 20.0),
//                       child: Text(
//                           "Swipe up if you're not interested in this type of content"),
//                     )
//                   ],
//                 ),
//           ),
//           ),
//           Positioned(
//               right: 20,
//               bottom: 30,
//               child: Container(
//                   width: 100,
//                   child: RaisedButton(
//                     color: Colors.green,
//                     onPressed: () {
//                       Navigator.pushReplacement(context,
//                           MaterialPageRoute(builder: (context) => RightSwipe()));
//                     },
//                     child: Text("Next"),
//                   ))
//           )
//         ],
//       ),
//     );
//   }
// }
// class RightSwipe extends StatefulWidget {

//   @override
//   _RightSwipe createState() => _RightSwipe();
// }

// class _RightSwipe extends State<RightSwipe> {
//   InstructionBloc instructionBloc;
//   @override
//   Widget build(BuildContext context) {
//     instructionBloc = InstructionBloc();

//     return Scaffold(
//       backgroundColor: Colors.white,
//       appBar: AppBar(
//         backgroundColor: Colors.white,
//         centerTitle: true,
//         title:  IconButton(
//           iconSize: 180,
//           icon: new Image.asset(
//             'assets/images/expfaces.png',
//             color: Colors.black,),
//         ),
//       ),
//       body:Stack(
//         children: <Widget>[
//           Center(child:Container(

//             child: Column(
//               mainAxisAlignment: MainAxisAlignment.center,
//               mainAxisSize: MainAxisSize.min,
//                   children: <Widget>[

//                     Container(
//                         child: Center(child:Column(
//                           children: <Widget>[
//                             IconButton(
//                               iconSize: 160,
//                               icon: new Image.asset(
//                                 'assets/images/swipe1.png',
//                               ),
//                             ),
//                           ],
//                         ))),
//                     Container(
//                       padding: EdgeInsets.only(left: 20,right: 20.0),
//                       child: Text("Swipe RIGHT if you LIKE it"),
//                     )
//                   ],
//                 ),
//           ),
//           ),
//           Positioned(
//               right: 20,
//               bottom: 30,
//               child: Container(
//                   width:100,
//                   child: RaisedButton(
//                     color: Colors.green,
//                     onPressed: () {
//                       Navigator.pushReplacement(context,
//                           MaterialPageRoute(builder: (context) => LeftSwipePage()));
//                     },
//                     child: Text("Next"),
//                   ))
//           )
//         ],
//       ),
//     );
//   }
// }
// class LeftSwipePage extends StatefulWidget {

//   @override
//   _LeftSwipePage createState() => _LeftSwipePage();
// }

// class _LeftSwipePage extends State<LeftSwipePage> {
//   InstructionBloc instructionBloc;

//   @override
//   Widget build(BuildContext context) {
//     instructionBloc = InstructionBloc();

//     return Scaffold(
//       backgroundColor: Colors.white,
//       appBar: AppBar(
//         backgroundColor: Colors.white,
//         centerTitle: true,
//         title:  IconButton(
//           iconSize: 180,
//           icon: new Image.asset(
//             'assets/images/expfaces.png',
//             color: Colors.black,),
//         ),
//       ),
//       body:leftSwipe(context),
//     );
//   }
//   Widget leftSwipe(BuildContext context){
//     return Stack(
//       children: <Widget>[
//         Center(child:Container(

//           child:Column(
//             mainAxisAlignment: MainAxisAlignment.center,
//             mainAxisSize: MainAxisSize.min,
//                 children: <Widget>[

//                   Container(
//                       child: Center(child:Column(
//                         children: <Widget>[
//                           IconButton(
//                             iconSize: 160,
//                             icon: new Image.asset(
//                               'assets/images/swipe1.png',
//                             ),
//                           ),
//                         ],
//                       ))),
//                   Container(
//                     padding: EdgeInsets.only(left: 20,right: 20.0),
//                     child: Text("Swipe LEFT if you DON'T LIKE it"),
//                   )
//                 ],
//               ),
//         ),
//         ),
//         Positioned(
//             right: 20,
//             bottom: 30,
//             child: Container(
//                 width:100,
//                 child: RaisedButton(
//                   color: Colors.green,
//                   onPressed: () {
//              instructionBloc.gotoFeedScreen(context:context);

//                   },
//                   child: Text("Next"),
//                 ))
//         )
//       ],
//     );
//   }
// }