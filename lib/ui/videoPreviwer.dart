import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:video_player/video_player.dart';
import 'package:xposure/ui/videoDescribtionPage.dart';
import 'package:xposure/constants/strings.dart' as Constants;
import 'package:xposure/constants/images.dart' as IMAGES;

class VideoPreviewer extends StatefulWidget {
  String url;
  String userId;
  String duration;
  VideoPreviewer(this.url, this.userId, this.duration);
  @override
  _VideoPreviewer createState() => new _VideoPreviewer();
}

class _VideoPreviewer extends State<VideoPreviewer>
    with SingleTickerProviderStateMixin {
  bool isInitialized = false;
  String videoPath;
  VideoPlayerController videoPlayerController;
  String userId;
  String duration = null;
  bool manualPause = true;
  static const platform = const MethodChannel(Constants.VIDEO_STREAM);

  @override
  dispose() {
    videoPlayerController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    var file = new File(widget.url);
    videoPlayerController = VideoPlayerController.file(file)
      ..initialize().then((_) {
        isInitialized = true;
        videoPlayerController.setLooping(true);
        duration = videoPlayerController.value.duration.toString();
        videoPlayerController.pause();
        setState(() {});
      });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([]);
    return WillPopScope(child:  Scaffold(
      body: Stack(fit: StackFit.expand, children: <Widget>[
        isInitialized
            ? GestureDetector(
                onTap: () {
                  setState(() {
                    // If the video is playing, pause it.
                    if (videoPlayerController.value.isPlaying) {
                      videoPlayerController.pause();
                      manualPause = true;
                    } else {
                      // If the video is paused, play it.
                      videoPlayerController.play();
                      manualPause = false;
                    }
                  });
                },
                child: Stack(children: <Widget>[
                  VideoPlayer(videoPlayerController),
                  Visibility(
                    visible: manualPause,
                    child: ButtonTheme(
                      height: MediaQuery.of(context).size.height,
                      minWidth: MediaQuery.of(context).size.width,
                      child: Container(
                        padding: EdgeInsets.all(60.0),
                        child: Center(
                          child: IconButton(
                            iconSize: 50,
                            icon: Image.asset(
                                IMAGES.PLAYBACK,
                                height: 50,
                                width: 50),
                          ),
                        ),
                      ),
                    ),
                  ),
                ])
                )
            : Container(
                color: Colors.black,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      children: <Widget>[CircularProgressIndicator()],
                    )
                  ],
                )),
      ]),
      floatingActionButton: Padding(
        padding: const EdgeInsets.only(left: 30.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
             Container(
                  height: 70,
                  width: 70,
                  child: Center(
                      child: IconButton(
                    iconSize: 50,
                    icon: new Image.asset(IMAGES.REDO),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  )),
            ),
             Container(
                  height: 70,
                  width: 70,
                  child: Center(
                      child: IconButton(
                          iconSize: 50,
                          icon: new Image.asset(IMAGES.UPLOADED),
                          onPressed: () async {
                            Navigator.pop(context);
                            Navigator.of(context).push(MaterialPageRoute(
                                builder: (context) => VideoDescribtionPage(
                                      url: widget.url,
                                      screen: Constants.VIDEO_RECORDER,
                                    )));
                          })),
            )
          ],
        ),
      ),
    ),onWillPop: (){Navigator.pop(context);},);
  }
}
