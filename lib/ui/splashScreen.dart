import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:logger/logger.dart';
import 'package:xposure/models/USER.dart';
import 'package:xposure/ui/feedScreen.dart';
import 'package:xposure/httpUtils/api.dart';
import 'package:xposure/services/feedVideoService.dart';
import 'package:video_player/video_player.dart';
import 'package:screen/screen.dart';
import 'package:xposure/constants/methods.dart';
import 'package:xposure/constants/images.dart' as IMAGES;
import 'package:xposure/constants/videos.dart' as VIDEOS;
import 'package:xposure/constants/strings.dart' as Constants;
import 'package:xposure/utils/preferenceManager.dart';
import 'package:permission/permission.dart';

class SplashScreen extends StatefulWidget {
  @override
  SplashScreenState createState() => new SplashScreenState();
}

class SplashScreenState extends State<SplashScreen>
    with SingleTickerProviderStateMixin, WidgetsBindingObserver {
  bool logoVisible = true;
  var _visible = false;
  bool getRespone = false;
  String userId = "";
  FeedVideoService feedVideoService = FeedVideoService();
  ApiProvider _apiProvider = ApiProvider();
  VideoPlayerController videoPlayerController;
  VideoPlayerController videoPlayerController1;
  var response;
  bool initilized = false;
  final logger = Logger();
  AnimationController animationController;
  Animation<double> animation;

  @override
  void initState() {
    permission();
    feedList();
    super.initState();
  }

  permission() async {
    if (Platform.isAndroid) {
      List<PermissionName> permissionNames = [];
      permissionNames.add(PermissionName.Camera);
      permissionNames.add(PermissionName.Storage);
      permissionNames.add(PermissionName.Microphone);
      List permissionStatus =
          await Permission.getPermissionsStatus(permissionNames);
      List<PermissionName> deniedPermissionList = List<PermissionName>();
      List<PermissionName> allowedPermissionList = List<PermissionName>();
      permissionStatus.forEach((e) {
        if (e.permissionStatus != PermissionStatus.allow) {
          deniedPermissionList.add(e.permissionName);
        } else if (e.permissionStatus == PermissionStatus.allow) {
          allowedPermissionList.add(e.permissionName);
        }
      });
      if (allowedPermissionList.length == 3) {}
      if (deniedPermissionList.length > 0) {
        await Permission.requestPermissions(deniedPermissionList);
      }
    }
  }

//initial api call
  feedList() {
    getFeedListData().then((_) async {
      changeView();
    });
  }

//feed get api call
  Future<void> getFeedListData() async {
    userId = await getUserId();
    var object = await PreferenceManager.getInstance().getCurrentUserObject();
    //json.decode(object);
    User user = User.fromJson(json.decode(object));
    ApplicationConstants.userId = user.userId;
    ApplicationConstants.photoUrl = user.photoUrl;
    ApplicationConstants.userName = user.name;
    await _apiProvider.getFeedVideos(userId, 1).then((value) {
      response = value;
    });
    logger.d(">>>>>>>>>-------RESP$response");
  }

//after getting response initialize video in feed screen and hide the logo and start playing the splash video
  changeView() {
    setState(() {
      videoPlayerController1 = VideoPlayerController.network(
          response[Constants.DATA][0][Constants.OBJECT_URL]);
      try {
        videoPlayerController1.initialize()
          ..then((_) {
            setState(() {
              logoVisible = false;
              initilized = true;
              videoPlayerController1.setLooping(true);
            });
          });
      } catch (e) {
        logger.e("-----SPLASHEXCEPTION---->$e");
      }

      videoPlayerController = VideoPlayerController.asset(VIDEOS.SPLASH)
        ..initialize().then((_) {
          setState(() {
            _visible = true;
          });
        });
      videoPlayerController.addListener(listener);
    });
  }

  //keep on the screen during app run
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    switch (state) {
      case AppLifecycleState.resumed:
        break;
      case AppLifecycleState.inactive:
        // TODO: Handle this case.
        break;
      case AppLifecycleState.paused:
        // TODO: Handle this case.
        break;
      case AppLifecycleState.detached:
        // TODO: Handle this case.
        break;
    }
    if (state == AppLifecycleState.resumed) {
      Screen.keepOn(true);
    }
  }

// ontap to navigate  feedscreen
  void navigationPage() {
    if (initilized && response != null) {
      Navigator.of(context).pushReplacement(MaterialPageRoute(
          builder: (BuildContext context) => (FeedScreen(
              initialData: response[Constants.DATA],
              videoPlayerController: videoPlayerController1))));
    } else {
      logger.e('No RESPONSE----------->');
    }
  }

  @override
  dispose() {
    videoPlayerController.dispose();
    super.dispose();
  }

//video player listener
  listener() {
    if (response != null) {
      if (videoPlayerController.value.duration ==
              videoPlayerController.value.position &&
          response != null &&
          initilized) {
        Navigator.of(context).pushReplacement(MaterialPageRoute(
            builder: (BuildContext context) => (FeedScreen(
                initialData: response[Constants.DATA],
                videoPlayerController: videoPlayerController1))));
      }
    } else {
      logger.e('No DATA----------->');
    }
  }

  @override
  Widget build(BuildContext context) {
    Screen.keepOn(true);
    SystemChrome.setEnabledSystemUIOverlays([]);
    if (logoVisible == false) {
      videoPlayerController.play();
    }
    return Scaffold(
      body: logoVisible == true
          ? Center(
              child: SizedBox(
              width: MediaQuery.of(context).size.width / 4,
              height: MediaQuery.of(context).size.height / 8,
              child: Image.asset(
                IMAGES.LOGO,
                fit: BoxFit.cover,
              ),
            ))
          : GestureDetector(
              onTap: () => navigationPage(),
              child: Stack(fit: StackFit.expand, children: <Widget>[
                VideoPlayer(videoPlayerController),
                _visible ? Image.asset(IMAGES.INTRO) : Container()
              ]),
            ),
    );
  }
}
