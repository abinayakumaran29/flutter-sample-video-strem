import 'dart:io';
import 'package:dio/dio.dart';
import 'package:logger/logger.dart';
import 'package:shared_preferences/shared_preferences.dart';

String uploadPercentage;
final logger = Logger();

Future<FormData> FormData3(filePath) async {
  logger.d(filePath);
  return FormData.fromMap({
    "file": await MultipartFile.fromFile(filePath,
        filename: "2019_10_18_12_47_53.mp4"),
  });
}

getAllVideos() async {
  try {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String userId = await sharedPreferences.getString("userId");
    logger.d("api calling");
    final data = {'userId': userId};
    var dio = Dio();
    Response response = await dio.get(
        "",
        queryParameters: data);
    logger.d(response.data);
    return response.data["data"];
  } on Exception catch (e) {
    logger.e("Error while uploading the file: ");
    return [];
  }
}

uploadVideos() async {
  try {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String userId = await sharedPreferences.getString("userId");
    logger.d("api calling");
    final data = {'userId': userId, "pageNo": 1};
    var dio = Dio();
    Response response = await dio.get(
        "",
        queryParameters: data);
    logger.d(response.data);
    return response.data["data"];
  } on Exception catch (e) {
    logger.e("Error while uploading the file: " + e.toString());
    return [];
  }
}

feedResponse(reponse) async {
  try {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    String userId = await sharedPreferences.getString("userId");
    logger.d("api calling");
    final data = {
      'userId': userId,
      "videoId": reponse['videoId'],
      "favouriteStatus": reponse['favourite'],
      "videoStatus": reponse['videoStatus']
    };
    var dio = Dio();
    Response response = await dio.post(
        "",
        data: data);
    logger.d(response.data);
    return response.data["data"];
  } on Exception catch (e) {
    logger.e("Error while uploading the file: " + e.toString());
    return [];
  }
}
