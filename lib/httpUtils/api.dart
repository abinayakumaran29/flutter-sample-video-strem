import 'package:dio/dio.dart';
import 'package:xposure/api/api.dart';
import 'package:xposure/constants/methods.dart';
import 'package:xposure/httpUtils/httpsutils.dart';

import 'package:xposure/models/USER.dart';
import 'package:xposure/utils/preferenceManager.dart';
import 'package:path_provider/path_provider.dart';
import 'package:video_thumbnail/video_thumbnail.dart';

class ApiProvider {
  Future<Dio> _customDio() async {
    Dio dio = Dio();
    String accessId = await PreferenceManager.getInstance().getAccessId();
    dio.options.headers["Authorization"] = "Bearer " +
        accessId;
    return dio;
  }

  saveUser(User user) async {
    String path = baseUrl + 'login';
    try {
      var data = {
        "userId": user.userId.toString(),
        "name": user.name,
        "email": user.email,
        "photoUrl": user.photoUrl,
        "token": user.token,
      };
      Dio dio = Dio();
      //var _dio = await _customDio();
      Response response = await dio.post(path, data: data);

      if (response.statusCode == 200) {
        logger.d(">>>>>>>RESPONSEEEE${response.data['data']}");
        return response.data['profile'];
      }
    } catch (e) {
      return "saveuser operation failed";
    }
  }

  getFeedVideos(String userId, int pageNo) async {
    String path = baseUrl + 'feedVideo';
    try {
      var _dio = await _customDio();
      var data = {"userId": userId, "pageNo": pageNo};
      Response response = await _dio.get(path, queryParameters: data);
      if (response.statusCode == 200) {
        return response.data;
      }
    } catch (e) {
      logger.e(">>>>>>>$e");
      return [];
    }
  }
  historyVideos()async{
  try{

print(">>>>>>>>>>-----${ApplicationConstants.userId}");
var a = await getUploadVideos(ApplicationConstants.userId,1);
var b = await getLikedVideos(ApplicationConstants.userId,1);
var c = await gethistoryVideos(ApplicationConstants.userId,1);
if(a == null){
  a = [];
}
if(b == null){
  b = [];
}
if(c == null){
  c = [];
}
logger.d(">>>>>>-----PRINTING-----UPLOAD---$a");
logger.d(">>>>>>-----PRINTING-----LIKED---$b");
logger.d(">>>>>>-----PRINTING-----HISTORY---$c");
var d= [a,b,c];
return d;
  }on Exception catch (e) {
      logger.e("Error while uploading the file: " + e.toString());
      return [];
    }
  }

  feedResponse(reponse) async {
    try {
      String userId = await PreferenceManager.getInstance().getCurrentUserId();
      final data = {
        'userId': userId,
        "videoId": reponse['videoId'],
        "favouriteStatus": reponse['favouriteStatus'],
        "videoStatus":
            reponse['videoStatus'] == null ? "null" : reponse['videoStatus']
      };
      var dio = await _customDio();
      print(">>>>>>>>>>POSTDATA@@@${data}");
      Response response = await dio.post(
          "",
          data: data);
      logger.d(response.data);
      return response.data["data"];
    } on Exception catch (e) {
      logger.e("Error while uploading the file: " + e.toString());
      return [];
    }
  }

  hashtagList(String values) async {
    try {
      print(">>>>>>>VALUEEEEEEEEEE$values");
      var dio = await _customDio();
      // print(">>>>>>>>>>POSTDATA@@@${data}");
      Response response = await dio.get(
          "",
          queryParameters: {"hashTags": values});
      logger.d(">>>>>---------RESPONSE${response.data['data']}");
      return response.data['data'];
    } catch (e) {}
  }

  getThumbNail(String url) async {
    final uint8list = await VideoThumbnail.thumbnailFile(
      video: url,
      thumbnailPath: (await getTemporaryDirectory()).path,
      imageFormat: ImageFormat.WEBP,
      maxHeight:
          150, // specify the height of the thumbnail, let the width auto-scaled to keep the source aspect ratio
      quality: 75,
    );
    print(">>>>>>>>>IAMHERE$uint8list");
    return uint8list;
  }

  getLikedVideos(String id,int pageNo)async{
    String path = baseUrl+"video-status/profile/"+id+"/likes";
    print(">>>>>>>>>----$pageNo-liked--$path--");
    try{
      Dio dio = Dio();
      Response response = await dio.get(path,queryParameters: {"pageNo":pageNo});
      if (response.data != null) {
        logger.d(">>>>>>>>>-----LIKEDRES-----${response.data}");
        return response.data['data']['Items'];
      }
    } catch (e) {
      logger.e(">>>>>>>>>-----fetching error while --$e");
    }
  }

  gethistoryVideos(String id,int pageNo)async{
    String path = baseUrl+"video-status/profile/"+id+"/history";

    try {
      Dio dio = Dio();
      Response response = await dio.get(path,queryParameters: {"pageNo":pageNo});
      if (response.data != null) {
        return response.data['data']['Items'];
      }
    }catch(e){
      logger.e(">>>>>>>>>-HISTROY--$path--fetching error while --$e");
    }
  }

  getfavouriteVideos(String id,int pageNo) async {
    String path = baseUrl + "video-status/profile/" + id + "/favourite";

    try {
      Dio dio = Dio();
      Response response = await dio.get(path,queryParameters: {"pageNo":pageNo});
      if (response.data != null) {
        return response.data['data']['Items'];
      }
    } catch (e) {
      logger.e(">>>>>>>>>-----fetching error while --$e");
    }
  }

  getUploadVideos(String id,int pageNo)async{
    String path = baseUrl+"user-videos/profile/"+id+"/mymedia";
    try{
      Dio dio = Dio();
      Response response = await dio.get(path,queryParameters: {"pageNo":pageNo});
      if (response.data != null) {
        logger.d(">>>>>>---------uploads-----DATA FETCHED----${response.data}");
        return response.data['data']['Items'];
      }
    } catch (e) {
      logger.e(">>>>>>>>>-----fetching error while --$e");
    }
  }
}
