import 'package:flutter/material.dart';

pageRouter(BuildContext context, dynamic screen) {
  Navigator.push(context, MaterialPageRoute(builder: (context) => screen));
}

progressingLoader(BuildContext context) {
  return showDialog<void>(
    barrierDismissible: false,
    context: context,
    builder: (BuildContext context) {
      return Center(
          child: Center(
              child: Container(child: CircularProgressIndicator()
                  )));
    },
  );
}
