import 'package:shared_preferences/shared_preferences.dart';

class PreferenceManager {

 static PreferenceManager preferenceManager;
SharedPreferences sharedPreferences;
    
    
   static PreferenceManager getInstance(){
      preferenceManager = PreferenceManager();
    return preferenceManager;
  }


  updateUserId(String userId) async {
    sharedPreferences = await SharedPreferences.getInstance();
    await sharedPreferences.setString('USERID',userId);
  }

  getCurrentUserId()async{
    sharedPreferences = await SharedPreferences.getInstance();
     return await sharedPreferences.getString('USERID');
  }

  updateUserObject( user) async {
    sharedPreferences = await SharedPreferences.getInstance();
    await sharedPreferences.setString('USEROBJECT',user);
  }

  getCurrentUserObject()async{
    sharedPreferences = await SharedPreferences.getInstance();
     return await sharedPreferences.getString('USEROBJECT');
  }

  updateAccessId(String accessId) async {
    sharedPreferences = await SharedPreferences.getInstance();
    await sharedPreferences.setString('ACCESSTOKEN',accessId);
  }

  getAccessId()async{
    sharedPreferences = await SharedPreferences.getInstance();
     return await sharedPreferences.getString('ACCESSTOKEN');
  }
}