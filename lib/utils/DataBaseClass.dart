// import 'package:sqflite/sqflite.dart';

// class DataBaseCrud{
//   Future<void> insertDog(FeedModel feedModel) async {
//     // Get a reference to the database.
//     final Database db = await database;

//     // Insert the FeedModel into the correct table. Also specify the
//     // `conflictAlgorithm`. In this case, if the same feedModel is inserted
//     // multiple times, it replaces the previous data.
//     await db.insert(
//       'feedModels',
//       feedModel.toMap(),
//       conflictAlgorithm: ConflictAlgorithm.replace,
//     );
//   }

//   Future<List<FeedModel>> feedModels() async {
//     // Get a reference to the database.
//     final Database db = await database;

//     // Query the table for all The Dogs.
//     final List<Map<String, dynamic>> maps = await db.query('feedModels');

//     // Convert the List<Map<String, dynamic> into a List<FeedModel>.
//     return List.generate(maps.length, (i) {
//       return FeedModel(
//         id: maps[i]['id'],
//         name: maps[i]['name'],
//         age: maps[i]['age'],
//       );
//     });
//   }

//   Future<void> updateDog(FeedModel feedModel) async {
//     // Get a reference to the database.
//     final db = await database;

//     // Update the given FeedModel.
//     await db.update(
//       'feedModels',
//       feedModel.toMap(),
//       // Ensure that the FeedModel has a matching id.
//       where: "id = ?",
//       // Pass the FeedModel's id as a whereArg to prevent SQL injection.
//       whereArgs: [feedModel.id],
//     );
//   }

// Future<void> deleteDog(int id) async {
//     // Get a reference to the database.
//     final db = await database;

//     // Remove the FeedModel from the database.
//     await db.delete(
//       'feedModels',
//       // Use a `where` clause to delete a specific feedModel.
//       where: "id = ?",
//       // Pass the FeedModel's id as a whereArg to prevent SQL injection.
//       whereArgs: [id],
//     );
//   }

//   var fido = FeedModel(
//     id: 0,
//     name: 'Fido',
//     age: 35,
//   );
// }

// class FeedModel {
//   String videoId;
//   String objectUrl;
//   int videoStatus;
//   String duration;
//   bool favouriteStatus;
//   String mp4Urls;
//   String hlsUrl;
//   String dashUrl;

//   FeedModel(
//       {this.videoId,
//       this.objectUrl,
//       this.videoStatus,
//       this.duration,
//       this.favouriteStatus,
//       this.mp4Urls,
//       this.hlsUrl,
//       this.dashUrl});

//   FeedModel.fromJson(Map<String, dynamic> json) {
//     videoId = json['videoId'];
//     objectUrl = json['objectUrl'];
//     videoStatus = json['videoStatus'];
//     duration = json['duration'];
//     favouriteStatus = json['favouriteStatus'];
//     mp4Urls = json['mp4Urls'];
//     hlsUrl = json['hlsUrl'];
//     dashUrl = json['dashUrl'];
//   }

//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['videoId'] = this.videoId;
//     data['objectUrl'] = this.objectUrl;
//     data['videoStatus'] = this.videoStatus;
//     data['duration'] = this.duration;
//     data['favouriteStatus'] = this.favouriteStatus;
//     data['mp4Urls'] = this.mp4Urls;
//     data['hlsUrl'] = this.hlsUrl;
//     data['dashUrl'] = this.dashUrl;
//     return data;
//   }
// }
