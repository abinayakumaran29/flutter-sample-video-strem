import 'package:flutter/widgets.dart';
import 'package:video_player/video_player.dart';

class CustomVideoPlayer extends StatefulWidget {
  String url;
  CustomVideoPlayer(this.url);
  @override
  State<StatefulWidget> createState() {
    print(">>>>>>>>>hello");
    // TODO: implement createState
    return _CustomVideoPlayer();
  }
}

class _CustomVideoPlayer extends State<CustomVideoPlayer> {
  VideoPlayerController videoPlayerController;
  Future<void> _initializeVideoPlayerFuture;
  @override
  void initState() {
    // TODO: implement initState
    videoPlayerController = VideoPlayerController.network(widget.url);
    videoPlayerController.addListener(isVideoCompleted(videoPlayerController));
    _initializeVideoPlayerFuture = videoPlayerController.initialize();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Stack(children: <Widget>[
      Center(
        child: FutureBuilder(
            future: _initializeVideoPlayerFuture,
            builder: (context, snapshot) {
              AspectRatio(
                aspectRatio: videoPlayerController.value.aspectRatio,
                child: VideoPlayer(videoPlayerController),
              );
            }),
      )
    ]);
  }
  isVideoCompleted(VideoPlayerController videoPlayerController) {
    print(">>>>>>>>>${videoPlayerController.position}");
    print(">>>>>>>>>${videoPlayerController.value.duration}");
    if (videoPlayerController.value.duration ==
        videoPlayerController.position) {
      print(">>>>>>>>>>>>completed");
    }
  }
}
