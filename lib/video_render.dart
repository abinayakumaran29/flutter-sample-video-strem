import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

class VideoPlayerRender extends StatefulWidget{

  //String URL;
  //VideoPlayerRender({this.URL, Key key}): super(key : key);

  VideoPlayerRender({
    this.pageIndex,
    this.currentPageIndex,
    this.isPaused,
    this.URL
  });

  final int pageIndex;
  final int currentPageIndex;
  final bool isPaused;
  final String URL;

  @override
  _VideoPlayerState createState() => _VideoPlayerState();
}

class _VideoPlayerState extends State<VideoPlayerRender> {
  VideoPlayerController _controller;
  bool initialized = false;
  bool manualPause = false;
  bool loadingSpinner = true;
  Future<void> _initializeVideoPlayerFuture;


  // listener(){
  //   if(_controller.value.position == _controller.value.duration){
  //     _controller.pause();
  //   print(">>>POSITION>>>>${_controller.value.position}");
  //   print(">>>DURATION>>>>${_controller.value.duration}");
  //   }
  // }

  @override
  void initState() {
    _controller = VideoPlayerController.network(widget.URL);
    //_controller.addListener(listener);
    _initializeVideoPlayerFuture = _controller.initialize().then((_){
      setState(() {
         _controller.setLooping(true);
        initialized = true;
        manualPause = false;
      });
    });

    super.initState();

  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    //throw UnimplementedError();

    /*return Center(
      child: _controller.value.initialized
          ? AspectRatio(
        aspectRatio: _controller.value.aspectRatio,
        child: VideoPlayer(_controller),
      )
          : Container(
        color: Colors.black,
      ),
    );*/

    /*if (widget.pageIndex == widget.currentPageIndex &&
        !widget.isPaused &&
        initialized) {
      _controller.play();
    } else {
      _controller.pause();
    }

    return Center(
      child: _controller.value.initialized
          ? AspectRatio(
        aspectRatio: _controller.value.aspectRatio,
        child: VideoPlayer(_controller),
      )
          : Container(
        color: Colors.black,
      ),
    );*/

    /*return Center(
      child: FutureBuilder(
        future: _initializeVideoPlayerFuture,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            // If the VideoPlayerController has finished initialization, use
            // the data it provides to limit the aspect ratio of the video.
            if (widget.pageIndex == widget.currentPageIndex &&
                !widget.isPaused &&
                initialized) {
              _controller.play();
            } else {
              _controller.pause();
            }

            return AspectRatio(
              aspectRatio: _controller.value.aspectRatio,
              // Use the VideoPlayer widget to display the video.
              child: VideoPlayer(_controller),
            );
          } else {
            // If the VideoPlayerController is still initializing, show a
            // loading spinner.
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
    );*/
    return Stack(
      children: <Widget>[
        Center(
          child: FutureBuilder(
            future: _initializeVideoPlayerFuture,
            builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                loadingSpinner = false;
                // If the VideoPlayerController has finished initialization, use
                // the data it provides to limit the aspect ratio of the video.
                if (widget.pageIndex == widget.currentPageIndex &&
                    !widget.isPaused &&
                    initialized && !manualPause) {
                  _controller.play();
                } else {
                  _controller.pause();
                  _controller.seekTo(Duration(seconds: 0));
                }
                return AspectRatio(
                  aspectRatio:  _controller.value.aspectRatio,
                  // Use the VideoPlayer widget to display the video.
                  child: VideoPlayer(_controller),
                );
              } else {
                // If the VideoPlayerController is still initializing, show a
                // loading spinner.
                loadingSpinner = true;
                return Center(child: CircularProgressIndicator());
              }
            },
          ),
        ),
    //     GestureDetector(
    //         onTap: (){
    // setState(() {
    // // If the video is playing, pause it.
    // if (_controller.value.isPlaying) {
    // manualPause = true;
    // _controller.pause();
    // } else {
    // // If the video is paused, play it.
    // manualPause = false;
    // _controller.play();
    // }
    // });
    //         },
    //         child: ButtonTheme(
    //           height: MediaQuery.of(context).size.height,
    //           minWidth: MediaQuery.of(context).size.width,
    //             child: Container(
    //               padding: EdgeInsets.all(60.0),
    //               child: Center(
    //               child: Icon(
    //                 Icons.play_arrow,
    //                 color: manualPause && !loadingSpinner ? Colors.white : Colors.transparent,
    //                 size: 80.0,
    //               )
    //             ),
    //             ),
    //         ),
    //         /*ButtonTheme(
    //             height: 100.0,
    //             minWidth: 200.0,
    //             child: RaisedButton(
    //               padding: EdgeInsets.all(60.0),
    //               color: Colors.transparent,
    //               textColor: Colors.white,
    //               onPressed: () {
    //                 // Wrap the play or pause in a call to `setState`. This ensures the
    //                 // correct icon is shown.
    //                 setState(() {
    //                   // If the video is playing, pause it.
    //                   if (_controller.value.isPlaying) {
    //                     manualPause = true;
    //                     _controller.pause();
    //                   } else {
    //                     // If the video is paused, play it.
    //                     manualPause = false;
    //                     _controller.play();
    //                   }
    //                 });
    //               },
    //               child: Icon(
    //                 _controller.value.isPlaying ? Icons.pause : Icons.play_arrow,
    //                 size: 120.0,
    //               ),
    //             ))*/
    //     )
      ],
    );



  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

}