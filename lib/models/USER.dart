import 'dart:core';

import 'dart:core';

class User {
  String userId;
  String name;
  String email;
  String photoUrl;
  String authToken;
  String token;

  User(
      {this.userId,
      this.name,
      this.email,
      this.photoUrl,
      this.authToken,
      this.token});

  User.fromJson(Map<String, dynamic> json) {
    userId = json['userId'];
    name = json['name'];
    email = json['email'];
    photoUrl = json['photoUrl'];
    authToken = json['authToken'];
    token = json['token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['userId'] = this.userId;
    data['name'] = this.name;
    data['email'] = this.email;
    data['photoUrl'] = this.photoUrl;
    data['authToken'] = this.authToken;
    data['token'] = this.token;
    return data;
  }

     String toString() {
        return "Nnx{" +
                "userId='" + userId + '\'' +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", photoUrl='" + photoUrl + '\'' +
                ", authToken='" + authToken + '\'' +
                ", token='" + token + '\'' +
                '}';
    }

     String getUserId() {
        return userId;
    }

     void setUserId(String userId) {
        this.userId = userId;
    }

     String getName() {
        return name;
    }

     void setName(String name) {
        this.name = name;
    }

     String getEmail() {
        return email;
    }

     void setEmail(String email) {
        this.email = email;
    }

     String getPhotoUrl() {
        return photoUrl;
    }

     void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

     String getAuthToken() {
        return authToken;
    }

     void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

     String getToken() {
        return token;
    }

     void setToken(String token) {
        this.token = token;
    }
}