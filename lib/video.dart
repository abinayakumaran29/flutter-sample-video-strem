// import 'dart:io';

// import 'package:flutter/services.dart';
// import 'package:flutter/material.dart';
// import 'package:xposure/ui/multiple_page.dart';
// import 'package:google_sign_in/google_sign_in.dart';
// import 'package:shared_preferences/shared_preferences.dart';
// import 'package:file_picker/file_picker.dart';
// import 'package:xposure/api/api.dart';
// import 'package:percent_indicator/circular_percent_indicator.dart';
// import 'package:xposure/CameraWidget.dart';
// import 'package:xposure/histroyPage.dart';
// import 'package:xposure/video_render.dart';

// //void main() => runApp(MaterialApp(home: MyApp()));
// SharedPreferences sharedPreferences;
// String userToken;

// class VideoGallery extends StatefulWidget {
//   @override
//   State<StatefulWidget> createState() {
//     return _MyHomePage();
//   }
// }

// initialSharredPref() async {
//   sharedPreferences = await SharedPreferences.getInstance();
//   userToken = await sharedPreferences.getString("userId");
// }

// class _MyHomePage extends State<VideoGallery> {
//   var dataVideo;
//   var list = [];
//   bool isUploading = false;
//   String videoPath;
//   String uploadPercentage;
//   static const platform = const MethodChannel('video_stream');
//   static const tokenPlatform = const MethodChannel('userId');
//   static const refreshPlatform = const MethodChannel('refreshTapsPage');
//   final GoogleSignIn _googleSignIn = GoogleSignIn();
//   bool homePage = true;

//   @override
//   void initState() {
//     super.initState();
   
//     refreshPlatform.setMethodCallHandler(nativeMethodCallHandler);
//     dataVideo = getAllVideos();
//   }

//   Future<dynamic> nativeMethodCallHandler(MethodCall methodCall) async {
//     switch (methodCall.method) {
//       case 'refreshTaps':
//         setState(() {
//           uploadPercentage = "Uploading";
//           isUploading = false;
//         });

//         return;
//       default:
//     }
//   }

  
//   void getFilePath() async {
//     try {
//       String filePath = await FilePicker.getFilePath(type: FileType.video);

//       print("mass da${filePath}");
//       videoPath = filePath;
    
//       if (filePath == '' || filePath == null) {
//         return;
//       }
//       setState(() {
//         uploadPercentage = "Uploading";
//         isUploading = true;
//       });
//       initialSharredPref();
       

        
//       final Map<String, dynamic> params = <String, dynamic>{
//         'filePath': filePath,
//         'userId': userToken,
//       };
//       await platform.invokeMethod("videoInfo", params);

//       setState(() {});
//     } on Exception catch (e) {
//       print("Error while picking the file: " + e.toString());
//     }
//   }

//   Future<void> refreshPage() async {
//     setState(() {
//     });
//   }

//   Future<Null> close() {
//     exit(0);
//   }

//   Future<bool> _exitApp(BuildContext context) {
//     return showDialog(
//           context: context,
//           child: new AlertDialog(
//             title: new Text('Do you want to exit this application?'),
//             actions: <Widget>[
//               new FlatButton(
//                 onPressed: () => Navigator.of(context).pop(false),
//                 child: new Text('No'),
//               ),
//               new FlatButton(
//                 onPressed: () => exit(0),
//                 child: new Text('Yes'),
//               ),
//             ],
//           ),
//         ) ??
//         false;
//   }

//   @override
//   Widget build(BuildContext context) {
//     //final controller = PreloadPageController(initialPage: 0);
//     initialSharredPref();
//     return WillPopScope(
//       onWillPop: () => _exitApp(context),
//       child: Scaffold(
//         body: isUploading
//             ? Center(
//                 child: new CircularPercentIndicator(
//                   radius: 100.0,
//                   lineWidth: 5.0,
//                   percent: 1.0,
//                   center: new Text(uploadPercentage),
//                   progressColor: Colors.green,
//                 ),
//               )
//             : FutureBuilder(
//                 future: dataVideo,
//                 builder: (BuildContext context, AsyncSnapshot snapshot) {
//                   return snapshot.connectionState != ConnectionState.done
//                       ? Center(child: CircularProgressIndicator())
//                       : RefreshIndicator(
//                           onRefresh: refreshPage,
//                           child: Stack(children: <Widget>[
//                             Container(
//                               child: MultiplePage(
//                                   snapshot),
//                             ),
//                             Align(
//                                 alignment: Alignment.bottomCenter,
//                                 child: Theme(
//                                     data: Theme.of(context).copyWith(
//                                         canvasColor: Colors.transparent),
//                                     child: BottomNavigationBar(
//                                       type: BottomNavigationBarType.fixed,
//                                       items: [
//                                         new BottomNavigationBarItem(
//                                             icon: new SizedBox(
//                                               height: 60,
//                                               width: 60,
//                                               child: StreamBuilder(
//                                                   builder: (context, snapshot) {
//                                                 return Container(
//                                                   decoration: BoxDecoration(
//                                                       color: Colors.white38,
//                                                       shape: BoxShape.circle),
//                                                   child: new IconButton(
//                                                       icon: new Image.asset(
//                                                         'assets/images/image2.png',
//                                                       ),
//                                                       onPressed: () {}),
//                                                 );
//                                               }),
//                                             ),
//                                             title: new Text(
//                                               "",
//                                               style: new TextStyle(fontSize: 0),
//                                             )),
//                                         new BottomNavigationBarItem(
//                                             icon: new SizedBox(
//                                               height: 80,
//                                               width: 80,
//                                               child: Container(
//                                                 decoration: BoxDecoration(
//                                                     color: Colors.white38,
//                                                     shape: BoxShape.circle),
//                                                 child: new IconButton(
//                                                     icon: new Image.asset(
//                                                         'assets/images/plus.png'),
//                                                     onPressed: () {
//                                                       Navigator.push(
//                                                         context,
//                                                         MaterialPageRoute(
//                                                             builder: (context) =>
//                                                                 VideoRecorderExample()),
//                                                       );
//                                                     }),
//                                               ),
//                                             ),
//                                             title: new Text(
//                                               "",
//                                               style: new TextStyle(fontSize: 0),
//                                             )),
//                                         new BottomNavigationBarItem(
//                                             icon: new SizedBox(
//                                               height: 60,
//                                               width: 60,
//                                               child: StreamBuilder(
//                                                   builder: (context, snapshot) {
//                                                 return Container(
//                                                   decoration: BoxDecoration(
//                                                       color: Colors.white38,
//                                                       shape: BoxShape.circle),
//                                                   child: new IconButton(
//                                                       icon: new Image.asset(
//                                                         'assets/images/image3.png',
//                                                       ),
//                                                       onPressed: () {
//                                                         //VideoPlayerRender(
//                                                           // isPaused = true,
//                                                         //);
//                                                         Navigator.push(
//                                                             context,
//                                                             MaterialPageRoute(
//                                                                 builder:
//                                                                     (context) =>
//                                                                         HistroyPage()));
//                                                       }),
//                                                 );
//                                               }),
//                                             ),
//                                             title: new Text(
//                                               "",
//                                               style: new TextStyle(fontSize: 0),
//                                             )),
//                                       ],
//                                     ))),
//                           ]),
//                         );
//                 }),
//       ),
//     );
//   }
// }
