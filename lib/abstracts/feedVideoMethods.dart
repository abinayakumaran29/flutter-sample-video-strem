import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

abstract class FeedVideoEvents{
  //void setContext(BuildContext contextBuildContext context);
  void initState();
  void liked(BuildContext context,GlobalKey<ScaffoldState> scaffold,var snapshot,int position);
  void disliked(BuildContext context, GlobalKey<ScaffoldState> scaffoldKey, snapshots, int position);
  void madefavourite(BuildContext context,var snapshots,int position);
  void notInterested(BuildContext context);
  void goToHistroyScreen(BuildContext context);
  void goToCamera(BuildContext context);
  void refresh(BuildContext context);
  void dispose(BuildContext context);
  void getFeedVideoList(BuildContext context);
}