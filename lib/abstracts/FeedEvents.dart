import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

abstract class FeedEvents{
  //void setContext(BuildContext contextBuildContext context);
  void initState();
  void liked(BuildContext context,GlobalKey<ScaffoldState> scaffold,var snapshot,int position);
  void disliked(BuildContext context,GlobalKey<ScaffoldState> scaffold,var snapshot,int position);
  void madefavourite(BuildContext context,var snapshot,int position);
  void notInterested(BuildContext context);
  void goToHistroyScreen(BuildContext context);
  void goToCamera(BuildContext context);
  void refresh(BuildContext context);
  void dispose(BuildContext context);
  void getFeedVideoList(BuildContext context, int page);
}