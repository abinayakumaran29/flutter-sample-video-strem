import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

abstract class CameraMethods{
  //void setContext(BuildContext contextBuildContext context);
  void initState();
  void goToPreviewScreen(BuildContext context);
  void flipCamera(BuildContext context);
  void startStopCamera(BuildContext context);
  void upload(BuildContext context);
  void preview(BuildContext context);
  void discard(BuildContext context);
}