import 'package:flutter/widgets.dart';

abstract class HistoryVideoMethods{
 void initState();
 void controller();
 void getHistoryVideoList(BuildContext context);
 void goToFeedScreen(BuildContext context);
 void playVideo(BuildContext context,String url);
 void dispose();
}