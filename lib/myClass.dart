import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:xposure/CameraWidget.dart';
import 'package:xposure/api/api.dart';
import 'package:xposure/histroyPage.dart';
import 'package:xposure/video_render.dart';
import 'package:preload_page_view/preload_page_view.dart';

class VideoTohistroy extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _VideTohistroy();
  }
}

class _VideTohistroy extends State<VideoTohistroy> {
  final scaffoldKey = GlobalKey<ScaffoldState>();
  StreamController streamController;
  PreloadPageController _controller;
  List lis;
  bool isOnPageTurning = false;
  int current = 0;
  var snapfdata;
  Object data = {};
  // final controller = PreloadPageController();
  @override
  void initState() {
    streamController = StreamController.broadcast();
    snapfdata = {};
    lis = List();
    _controller = PreloadPageController();
    _controller.addListener(scrollListener);
  }

  void postAction(videoId, videoStatus, favStat) {
    data = {
      'favourite': favStat,
      'videoId': videoId,
      'videoStatus': videoStatus
    };
    feedResponse(data);
  }

  void scrollListener() {
    // print(">>>>currentcurrentcurrentcurrent>>>>12344$current");
    if (_controller.hasClients) {
      if (isOnPageTurning &&
          _controller.page == _controller.page.roundToDouble()) {
        // setState(() {
          current = _controller.page.toInt();
          isOnPageTurning = false;
        // });
      } else if (!isOnPageTurning && current.toDouble() != _controller.page) {
        if ((current.toDouble() - _controller.page).abs() > 0.1) {
          // setState(() {
            isOnPageTurning = true;
          // });
        }
      }
    }
    // print(">>>>>isOnPageTurningisOnPageTurning>>>12344$isOnPageTurning");
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      key: scaffoldKey,
      body: FutureBuilder(
        future: getAllVideos(),
        builder: (coontext, snapShot) {
          return snapShot.connectionState == ConnectionState.waiting
              ? Center(
                  child: CircularProgressIndicator(),
                )
              : StreamBuilder(
                  stream: streamController.stream,
                  initialData: snapShot.data,
                  builder: (ccontext, snap) {
                    if (snap.hasData) {
                      snapfdata = snap.data;
                    }

                    return Stack(
                      children: <Widget>[
                        histroyPage(snapfdata["feedVideo"], scaffoldKey),
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: Theme(
                              data: Theme.of(context)
                                  .copyWith(canvasColor: Colors.transparent),
                              child: BottomNavigationBar(
                                elevation: 0,
                                type: BottomNavigationBarType.fixed,
                                items: [
                                  new BottomNavigationBarItem(
                                      icon: new SizedBox(
                                        height: 60,
                                        width: 60,
                                        child: StreamBuilder(
                                            builder: (context, snapshot) {
                                          return Container(
                                            decoration: BoxDecoration(
                                                color: Colors.white38,
                                                shape: BoxShape.circle),
                                            child: new IconButton(
                                                icon: new Image.asset(
                                                  'assets/images/image2.png',
                                                ),
                                                onPressed: () {}),
                                          );
                                        }),
                                      ),
                                      title: new Text(
                                        "",
                                        style: new TextStyle(fontSize: 0),
                                      )),
                                  new BottomNavigationBarItem(
                                      icon: new SizedBox(
                                        height: 80,
                                        width: 80,
                                        child: Container(
                                          decoration: BoxDecoration(
                                              color: Colors.white38,
                                              shape: BoxShape.circle),
                                          child: new IconButton(
                                              icon: new Image.asset(
                                                  'assets/images/plus.png'),
                                              onPressed: () {
                                                Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          VideoRecorderExample()),
                                                );
                                              }),
                                        ),
                                      ),
                                      title: new Text(
                                        "",
                                        style: new TextStyle(fontSize: 0),
                                      )),
                                  new BottomNavigationBarItem(
                                      icon: new SizedBox(
                                        height: 60,
                                        width: 60,
                                        child: StreamBuilder(
                                            builder: (context, snapshot) {
                                          return Container(
                                            decoration: BoxDecoration(
                                                color: Colors.white38,
                                                shape: BoxShape.circle),
                                            child: new IconButton(
                                                icon: new Image.asset(
                                                  'assets/images/image3.png',
                                                ),
                                                onPressed: () {
                                                  // Navigator.push(
                                                  //     context,
                                                  //     MaterialPageRoute(
                                                  //         builder: (context) =>
                                                  //             HistroyPage(snapfdata[
                                                  //                 "histroy"])));
                                                }),
                                          );
                                        }),
                                      ),
                                      title: new Text(
                                        "",
                                        style: new TextStyle(fontSize: 0),
                                      )),
                                ],
                              )),
                        )
                      ],
                    );
                  },
                );
        },
      ),
    );
  }

  histroyPage(var feedVideos, GlobalKey<ScaffoldState> scaffold) {
    var videoId = null;
    var videoStatus = null;
    var favStat = null;
    StreamController controller = StreamController.broadcast();
    //_controller.addListener(scrollListener(_controller));
    // _controller.addListener(scrollListener(_controller));
    return GestureDetector(
        child: PreloadPageView.builder(
            preloadPagesCount: 5,
            scrollDirection: Axis.vertical,
            controller: _controller,
            itemCount: feedVideos.length,
            itemBuilder: (context, position) {
              bool isFav = feedVideos[position]["favouriteStatus"];
              favStat = feedVideos[position]["favouriteStatus"];
              videoStatus = feedVideos[position]['videoStatus'];
              videoId = feedVideos[position]['videoId'];
              return Stack(
                children: <Widget>[
                  Container(
                    color: Colors.black,
                    child: VideoPlayerRender(
                      pageIndex: position,
                      isPaused: isOnPageTurning,
                      currentPageIndex: current,
                      URL: feedVideos[position]["objectUrl"],
                    ),
                  ),
                  Positioned(
                      right: 35,
                      bottom: 100,
                      child: StreamBuilder(
                        initialData: feedVideos[position]["favouriteStatus"],
                        stream: controller.stream,
                        builder: (context, snapshot) {
                          return Column(
                            children: <Widget>[
                              Container(
                                decoration: BoxDecoration(
                                    color: Colors.white38,
                                    shape: BoxShape.circle),
                                child: IconButton(
                                  color: isFav == true
                                      ? Colors.amber
                                      : Colors.white,
                                  iconSize: 40,
                                  icon: new Image.asset(
                                    'assets/images/star.png',
                                    color: isFav == true
                                        ? Colors.amber
                                        : Colors.white,
                                  ),
                                  onPressed: () {
                                    if (isFav) {
                                      isFav = false;
                                      favStat=false;
                                    } else {
                                      isFav = true;
                                      favStat=true;
                                    }
                                    controller.sink.add(isFav);

                                    postAction(videoId, videoStatus, isFav);
                                  },
                                ),
                              ),
                              SizedBox(
                                height: 5,
                              ),
                              Container(
                                decoration: BoxDecoration(
                                    color: Colors.white38,
                                    shape: BoxShape.circle),
                                child: IconButton(
                                    color: isFav == true
                                        ? Colors.amber
                                        : Colors.white,
                                    iconSize: 40,
                                    icon: new Image.asset(
                                      'assets/images/image4.png',
                                    ),
                                    onPressed: () {}),
                              ),
                            ],
                          );
                        },
                      ))
                ],
              );
            }),
        onHorizontalDragEnd: (dragDetails) {
          if (dragDetails.primaryVelocity > 0) {
            scaffold.currentState.removeCurrentSnackBar();
            scaffold.currentState.showSnackBar(SnackBar(content: Text("Like")));
            videoStatus = 'like';
            postAction(videoId, videoStatus, favStat);
          } else if (dragDetails.primaryVelocity < 0) {
            scaffold.currentState.removeCurrentSnackBar();
            scaffold.currentState
                .showSnackBar(SnackBar(content: Text("Dislike")));
            videoStatus = 'dislike';
            postAction(videoId, videoStatus, favStat);
          }
        }
        );
  }
}
