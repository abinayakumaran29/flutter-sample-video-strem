import 'package:shared_preferences/shared_preferences.dart';

getUserId() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString("USERID");
  }

  class ApplicationConstants{
    static String userId ;
    static String photoUrl;
    static String userName;
    //static String userId;

  }