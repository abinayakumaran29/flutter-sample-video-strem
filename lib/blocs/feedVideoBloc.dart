// import 'dart:async';
// import 'package:flutter/material.dart';
// import 'package:xposure/abstracts/feedVideoMethods.dart';
// import 'package:xposure/services/feedVideoService.dart';
// import 'package:xposure/utils/preferenceManager.dart';
// import 'package:preload_page_view/preload_page_view.dart';
// import 'package:video_player/video_player.dart';
// import 'package:xposure/ui/historyScreen.dart';
// import 'package:xposure/CameraWidget.dart';

// class FeedVideoBloc extends FeedVideoEvents {
//   BuildContext context;
//   String userId;
//   bool isOnPageTurning = false;
//   int pageNo = 1;
//   bool isMoved = false;
//   var obj = null;
//   String videoId = null;
//   int current = 0;
//   var snapshot;
//   VideoPlayerController _controller;
//   StreamController feedVideoController = StreamController(onListen: () {
//     print(">>>>>paused");
//   });

//   StreamController videoStatuscontroller = StreamController.broadcast();
//   bool favStatus = false;
//   int videoStatus = 1;
//   //static FeedVideoBloc feedVideoBloc;
//   PreloadPageController preLoadpageController;
//   FeedVideoService feedVideoService = FeedVideoService();
//   StreamController videoPausePlaycontroller = StreamController.broadcast();
//   StreamController gesturecontroller = StreamController.broadcast();

//   void manualPause() {
//     isOnPageTurning = false;
//     videoPausePlaycontroller.sink.add("");
//   }

//   @override
//   void initState() {
//     // TODO: implement initState
//     preLoadpageController = PreloadPageController();
//     //preLoadpageController.addListener(scrollListener);
//   }

//   currentData(snapshot, position) {
//     this.snapshot = snapshot;
//     videoId = snapshot.data[0]['videoId'];
//     videoStatus = snapshot.data[0]['videoStatus'];
//     favStatus = snapshot.data[0]['favouriteStatus'];
//   }

//   @override
//   void disliked(BuildContext context, GlobalKey<ScaffoldState> scaffoldKey,
//       snapshots, int position) async {
//     // TODO: implement disliked
//     print(">>>>>>>>>$position");
//     videoId = snapshots['videoId'];
//     videoStatus = snapshots['videoStatus'];
//     favStatus = snapshots["favouriteStatus"];
//     var isliked = snapshot.data[position]['videoStatus'];
//     isOnPageTurning = false;
//     videoPausePlaycontroller.sink.add("");
//     if ((isliked == 10) || (isliked == 1)) {
//       scaffoldKey.currentState.removeCurrentSnackBar();
//       scaffoldKey.currentState.showSnackBar(SnackBar(
//           content: Text("disLike"), duration: Duration(milliseconds: 100)));
//       videoStatus = 0;
//       snapshot.data[position]['videoStatus'] = 0;
//       await doAction();
//       scaffoldKey.currentState.didChangeDependencies();
//       await nextPage();
//     } else {
//       scaffoldKey.currentState.removeCurrentSnackBar();
//       scaffoldKey.currentState.showSnackBar(SnackBar(
//           content: Text("You are already disliked"),
//           duration: Duration(milliseconds: 100)));
//       await nextPage();
//     }
//     isOnPageTurning = false;
//     current = obj != null &&
//             obj['data'].length - 1 != preLoadpageController.page.round()
//         ? preLoadpageController.page.round() + 1
//         : 0;
//     videoPausePlaycontroller.sink.add("");
//   }

//   @override
//   void dispose(BuildContext context) {
//     // TODO: implement dispose
//     preLoadpageController.dispose();
//     videoPausePlaycontroller.close();
//     videoStatuscontroller.close();
//     gesturecontroller.close();
//     feedVideoController.close();
//   }

//   @override
//   void getFeedVideoList(BuildContext context) async {
//     // TODO: implement getFeedVideoList
//     //progressingLoader(context);
//     userId = await PreferenceManager.getInstance().getCurrentUserId();
//     pageNo = ++pageNo;
//     obj = await feedVideoService.getFeedVideoRecords(userId, pageNo);
//     feedVideoController.sink.add(obj["data"]);
//     feedVideoController.done;
//   }

//   @override
//   void goToCamera(BuildContext context) async {
//     // TODO: implement goToCamera
//     await videoPausePlaycontroller.sink.add("goToNext");
//     print(">>>>>>>>play");
//     Navigator.push(
//         context,
//         MaterialPageRoute(
//           builder: (context) => VideoRecorderExample(),
//         )).then((_) {
//       videoPausePlaycontroller.sink.add("only one");
//     });
//   }

//   @override
//   void goToHistroyScreen(BuildContext context) async {
//     var name = ModalRoute.of(context).isActive;
//     await videoPausePlaycontroller.sink.add("goToNext");
//     var aa = await Navigator.push(
//         context,
//         MaterialPageRoute(
//           builder: (context) => HistoryVideo(),
//         )).then((_) {
//       print(">>>>>>NAVIGATED");
//       videoPausePlaycontroller.sink.add("only one");
//     });
//     print(">>>>>>>${name}>>>>>${aa}");
//   }

//   @override
//   void liked(BuildContext context, GlobalKey<ScaffoldState> scaffoldKey,
//       snapshots, int position) async {
//     // TODO: implement liked
//     print(">>>>>>>>>$position");
//     videoId = snapshots['videoId'];
//     videoStatus = snapshots['videoStatus'];
//     favStatus = snapshots["favouriteStatus"];
//     var isliked = snapshot.data[position]['videoStatus'];
//     isOnPageTurning = false;
//     videoPausePlaycontroller.sink.add("");
//     if ((isliked == 0) || (isliked == 1)) {
//       scaffoldKey.currentState.removeCurrentSnackBar();
//       scaffoldKey.currentState.showSnackBar(SnackBar(
//           content: Text("Like"), duration: Duration(milliseconds: 100)));
//       videoStatus = 10;
//       snapshot.data[position]['videoStatus'] = 10;
//       await doAction();
//       scaffoldKey.currentState.didChangeDependencies();
//       await nextPage();
//     } else if (isliked == 10) {
//       scaffoldKey.currentState.removeCurrentSnackBar();
//       scaffoldKey.currentState.showSnackBar(SnackBar(
//           content: Text("You are already liked"),
//           duration: Duration(milliseconds: 100)));
//       await nextPage();
//     }
//     isOnPageTurning = false;
//     current = obj != null &&
//             obj['data'].length - 1 != preLoadpageController.page.round()
//         ? preLoadpageController.page.round() + 1
//         : 0;
//     videoPausePlaycontroller.sink.add("");
//   }

//   nextPage() {
//     if (!isMoved) {
//       isMoved = true;
//       print(
//           ">>>>>>TOTALOBJ>${obj['data'].length}>>>${preLoadpageController.page.round()}");
//       new Timer(new Duration(milliseconds: 600), () {
//         preLoadpageController.animateToPage(
//           obj != null &&
//                   obj['data'].length - 1 != preLoadpageController.page.round()
//               ? preLoadpageController.page.round() + 1
//               : 0,
//           duration: Duration(milliseconds: 500),
//           curve: Curves.linear,
//         );
//         isMoved = false;
//       });
//     } else {
//       return;
//     }
//   }

//   move() {
//     isMoved = false;
//     preLoadpageController.animateToPage(
//       0,
//       duration: Duration(milliseconds: 500),
//       curve: Curves.linear,
//     );
//   }

//   doAction() async {
//     var data = {
//       "videoId": videoId,
//       "videoStatus": videoStatus,
//       "favouriteStatus": favStatus
//     };
//     await feedVideoService.doAction(data);
//   }

//   @override
//   void madefavourite(BuildContext context, snapshots, position) {
//     // TODO: implement madefavourite
//     favStatus = snapshots["favouriteStatus"];
//     if (favStatus) {
//       favStatus = false;
//     } else {
//       favStatus = true;
//     }
//     videoId = snapshots['videoId'];
//     videoStatus = snapshots['videoStatus'];

//     snapshot.data[position]['favouriteStatus'] = favStatus;
//     doAction();
//     videoStatuscontroller.sink.add("");
//   }

//   @override
//   void notInterested(BuildContext context) {
//     // TODO: implement notInterested
//   }

//   @override
//   void refresh(BuildContext context) {
//     // TODO: implement refresh
//   }

//   @override
//   void setContext(BuildContext contextBuildContext) {
//     // TODO: implement setContext
//   }

//   // @override
//   // void initState() {
//   //   feedVideoController = StreamController.broadcast();
//   //   feedVideoService = FeedVideoService();
//   //   preLoadpageController = PreloadPageController();
//   //   preLoadpageController.addListener(scrollListener);
//   //  // getFeedVideoList();
//   //   // TODO: implement initState
//   // }

//   // @override
//   // void getFeedVideoList() async{
//   //   // TODO: implement getFeedVideoList
//   //   print(">>>>>>>>step1");
//   //   progressingLoader(context);
//   //   print(">>>>>>>>step2");
//   //   userId = await PreferenceManager.getInstance().getCurrentUserId();
//   //   pageNo = ++pageNo;
//   //   var obj = feedVideoService.getFeedVideoRecords(userId, pageNo);
//   //   feedVideoController.sink.add(obj);
//   // }

// }
