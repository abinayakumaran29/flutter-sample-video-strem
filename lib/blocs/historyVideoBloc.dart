import 'dart:async';

import 'package:flutter/material.dart';
import 'package:xposure/abstracts/historyVideoBloc.dart';
import 'package:xposure/services/feedVideoService.dart';
import 'package:xposure/ui/singleVideoPlayer.dart';
import 'package:xposure/utils/genericfun.dart';
import 'package:video_player/video_player.dart';

class HistoryBloc extends HistoryVideoMethods {
  TabController tabController;
  StreamController videoWidgetStream = StreamController.broadcast();
  BuildContext context;
  String userId;
  bool isOnPageTurning = true;
  int pageNo = 1;
  int current = 0;
  bool favStatus = false;
  String videoStatus = null;
  VideoPlayerController videoPlayController;
  //static FeedVideoBloc feedVideoBloc;
  FeedVideoService feedVideoService = FeedVideoService();
  @override
  void controller() async {
    // TODO: implement controller
  }
  @override
  void getHistoryVideoList(BuildContext context) async {
    // TODO: implement getFeedVideoList
  }

  @override
  void goToFeedScreen(BuildContext context) async {
    Navigator.pop(context,"hello");
  }

  @override
  void initState() {
    // TODO: implement initState
  }

  // @override
  // void playVideo(BuildContext context,String url) {
  // TODO: implement playVideo

  @override
  void playVideo(BuildContext context, String url) {
    // TODO: implement playVideo
    //videoWidgetStream.sink.add("play");
    //progressingLoader(context);
    // videoPlayController = VideoPlayerController.network(url);
    // videoPlayController.initialize().then((_) {
      // pageRouter(context, SingleVideoPlayer(url));
    // });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    videoPlayController.dispose();
    videoWidgetStream.close();
    tabController.dispose();
  }
  //}

}
