import 'dart:async';

import 'package:flutter/material.dart';
import 'package:xposure/abstracts/FeedEvents.dart';
import 'package:xposure/services/feedVideoService.dart';
import 'package:xposure/utils/preferenceManager.dart';
import 'package:xposure/CameraWidget.dart';
import 'package:xposure/ui/historyScreen.dart';


class FeedBloc extends FeedEvents {

  List<dynamic> feedList = List();
  String userId;
  FeedVideoService feedVideoService = FeedVideoService();
  StreamController feedVideoController = StreamController();
  String videoId = null;
  int videoStatus = 1;
  bool favStatus = false;

  @override
  void dispose(BuildContext context) {
    // TODO: implement dispose

  }

  @override
  Future<void> getFeedVideoList(BuildContext context, int i) async {
    // TODO: implement getFeedVideoList
    userId = await PreferenceManager.getInstance().getCurrentUserId();
    var list = await feedVideoService.getFeedVideoRecords(userId, i);
    feedList.addAll(list['data']);
    feedVideoController.sink.add(feedList);
  }

  @override
  void goToCamera(BuildContext context) {
    // TODO: implement goToCamera
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => VideoRecorderExample(),
        ));
  }

  @override
  void goToHistroyScreen(BuildContext context) async{
    // TODO: implement goToHistroyScreen
    // await Navigator.push(
    //     context,
    //     MaterialPageRoute(
    //       builder: (context) => HistoryVideo(),
    //     ));
  }

  @override
  Future<void> initState() async {
    // TODO: implement initState
  }


  @override
  void madefavourite(BuildContext context,snapshot, int position) {
    // TODO: implement madefavourite
     print('-----snapshot=-----${snapshot}');
    // if (snapshot["favouriteStatus"]) {
    //   favStatus = false;
    // } else {
    //   favStatus = true;
    // }
    favStatus = snapshot["favouriteStatus"];
    videoId = snapshot['videoId'];
    videoStatus = snapshot['videoStatus'];
    doAction();
  }

  @override
  void notInterested(BuildContext context) {
    // TODO: implement notInterested
  }

  @override
  void refresh(BuildContext context) {
    // TODO: implement refresh
  }

  @override
  void liked(BuildContext context, GlobalKey<ScaffoldState> scaffold,snapshot, int position) {
    // TODO: implement liked
      videoId = snapshot['videoId'];
    videoStatus = snapshot['videoStatus'];
    favStatus = snapshot["favouriteStatus"];
    var isliked=snapshot['videoStatus'];
        if ((isliked == 0) || (isliked == 1)) {
      scaffold.currentState.removeCurrentSnackBar();
      scaffold.currentState.showSnackBar(SnackBar(
        behavior: SnackBarBehavior.floating,

      content: Text("Like"), duration: Duration(milliseconds: 100)));
      videoStatus = 10;
      snapshot['videoStatus'] = 10;
       doAction();
      scaffold.currentState.didChangeDependencies();
    } else if (isliked == 10) {
      scaffold.currentState.removeCurrentSnackBar();
      scaffold.currentState.showSnackBar(SnackBar(
        behavior: SnackBarBehavior.floating,
          content: Text("You are already liked"),
          duration: Duration(milliseconds: 100)));
           doAction();
    }
  }

   @override
  void disliked(BuildContext context, GlobalKey<ScaffoldState> scaffold,snapshot, int position) async{
    // TODO: implement liked
      videoId = snapshot['videoId'];
    videoStatus = snapshot['videoStatus'];
    favStatus = snapshot["favouriteStatus"];
    var isliked=snapshot['videoStatus'];
        if ((isliked == 10) || (isliked == 1)) {
      scaffold.currentState.removeCurrentSnackBar();
      scaffold.currentState.showSnackBar(SnackBar(
        behavior: SnackBarBehavior.floating,

      content: Text("Dislike"), duration: Duration(milliseconds: 100)));
      videoStatus = 0;
      snapshot['videoStatus'] =   0;
       doAction();
      scaffold.currentState.didChangeDependencies();
    } else {
      scaffold.currentState.removeCurrentSnackBar();
      scaffold.currentState.showSnackBar(SnackBar(
        behavior: SnackBarBehavior.floating,

          content: Text("You are already disliked"),
          duration: Duration(milliseconds: 100)));
           doAction();
    }
  }


   doAction() async {
    var data = {
      "videoId": videoId,
      "videoStatus": videoStatus,
      "favouriteStatus": favStatus
    };
    await feedVideoService.doAction(data);
  }

}

