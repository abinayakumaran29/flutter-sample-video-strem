import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:xposure/abstracts/loginMethods.dart';
import 'package:xposure/models/USER.dart';
import 'package:xposure/services/loginService.dart';
import 'package:xposure/ui/splashScreen.dart';
import 'package:xposure/utils/dialogPage.dart';
import 'package:xposure/utils/genericfun.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:xposure/utils/preferenceManager.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:xposure/utils/connection.dart';
import 'package:xposure/constants/strings.dart' as Constants;

class LoginBloc extends LoginMethods {
  final GoogleSignIn _googleSignIn = GoogleSignIn();
  LoginService loginService = LoginService();

  @override
  void gotoNextScreen({BuildContext context}) async {
    // TODO: implement gotoNextScreen
    progressingLoader(context);

    check().then((intenet) async {
      if (intenet != null && intenet) {
        // Internet Present Case
        GoogleSignInAccount googleUser = await _googleSignIn.signIn();
        if (googleUser != null) {
          final GoogleSignInAuthentication googleAuth =
              await googleUser.authentication;
          String token = await FirebaseMessaging().getToken();
          if (googleUser != null) {
            await PreferenceManager.getInstance().updateUserId(googleUser.id);
            User user = User();
            user.setUserId(googleUser.id);
            user.setEmail(googleUser.email);
            user.setPhotoUrl(googleUser.photoUrl);
            user.setToken(token);
            user.setName(googleUser.displayName);
            var respo = await loginService.saveUser(user);
            await PreferenceManager.getInstance()
                .updateAccessId(respo[Constants.AUTH_TOKEN]);
                await PreferenceManager.getInstance().updateUserObject(json.encode(user.toJson()));
              var ii=  await PreferenceManager.getInstance().getCurrentUserObject();

                print(">>>>>>>>>------JUSTPRINT----${json.decode(ii)}");
          }
          Navigator.pop(context);
          //pageRouter(context,SplashScreen());
          Navigator.pushReplacement(
              context, MaterialPageRoute(builder: (context) => SplashScreen()));
        } else {
          Navigator.pop(context);
        }
      } else {
        Navigator.pop(context);
        singleActionDialog(context, Constants.INTERNET_ERROR,
            Constants.PLEASE_CHECK_YOUR_INTERNET_CONNECTION, Constants.OK);
      }
    });
  }

  @override
  void initState() {
    // TODO: implement initState
  }
}
