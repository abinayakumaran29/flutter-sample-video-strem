import 'dart:async';

import 'package:flutter/material.dart';
import 'package:xposure/abstracts/cameraMethods.dart';
import 'package:camera/camera.dart';
import 'package:fluttertoast/fluttertoast.dart';

class CameraBloc extends CameraMethods {
  BuildContext context;
  CameraController controller;
  bool video = true;
  String videoPath;
  Stopwatch watch = Stopwatch();

  @override
  void initState() {
    // TODO: implement initState
  }

  @override
  void flipCamera(BuildContext context) {
    // TODO: implement flipCamera
  }

  @override
  void goToPreviewScreen(BuildContext context) {
    // TODO: implement goToPreviewScreen
    Navigator.pop(context);
  }

  @override
  void startStopCamera(BuildContext context) {
    // TODO: implement startStopCamera
  }

  @override
  void upload(BuildContext context) {
    // TODO: implement upload
  }

  @override
  void discard(BuildContext context) {
    // TODO: implement discard
    watch.reset();
    Navigator.pop(context);
  }

  @override
  void preview(BuildContext context) {
    Navigator.pop(context);
    // TODO: implement preview
  }
}
