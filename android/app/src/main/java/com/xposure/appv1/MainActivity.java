package com.xposure.appv1;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.common.GooglePlayServicesUtil;
import io.flutter.app.FlutterActivity;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugins.GeneratedPluginRegistrant;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;




public class MainActivity extends FlutterActivity {
  private static final String CHANNEL = "video_stream";
  private static final String REFRESH_CHANNEL = "refreshTapsPage";
    MethodChannel channel;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    GeneratedPluginRegistrant.registerWith(this);


    new MethodChannel(getFlutterView(), CHANNEL).setMethodCallHandler(
            new MethodChannel.MethodCallHandler() {
              @Override
              public void onMethodCall(MethodCall call, MethodChannel.Result result) {

                
                  AsyncTask<Void, Void, String> task = new AsyncTask<Void, Void, String>() {
                      @Override
                      protected String doInBackground(Void... params) {
                         // String token = null;

                          try{
                              channel = new MethodChannel(getFlutterView(), REFRESH_CHANNEL);


                              CognitoCachingCredentialsProvider credentialsProvider = new CognitoCachingCredentialsProvider(getApplicationContext(),
                                      "test", Regions.US_EAST_1);

                              AmazonS3 s3 = new AmazonS3Client(credentialsProvider);

                              s3.setRegion(Region.getRegion(Regions.US_EAST_1));
                              TransferUtility transferUtility = new TransferUtility(s3, MainActivity.this);
                             // String[] dataList=call.method.toString().split("##");
                              String filePath = call.argument("filePath");
                              String userId = call.argument("userId");
                              String duration = call.argument("duration");
                              String videoName = call.argument("videoName");
                              String describtion = call.argument("videoDescription");
                              ArrayList tags = new ArrayList();
                              tags.addAll(call.argument("hashTags"));
                             //ArrayList<String> tags = call.argument("hashTags");
                              System.out.println(">>>>>>>---123456-<<<<<"+tags);
                              System.out.println(">>>>>>>---123456-<))))))))<<<<"+describtion+"   "+userId);
                              //List<String> tags = new List<String>();
                             // List tags = call.argument("hashTags");
                              StringBuilder stringBuilder = new StringBuilder();
                              for(int i=0;i<tags.size();i++){
                                  System.out.println("00000000000000000000000000"+i);
                                  if(i != tags.size()-1) {
                                      stringBuilder.append(tags.get(i).toString().trim()+",");
                                  }else {
                                      stringBuilder.append(tags.get(i).toString().trim());
                                  }
                              }
                              System.out.println(">>>>>>>STRINGBUILDEr-------"+stringBuilder.toString());
                            // StringBuilder sb = new StringBuilder();
                            // for(int i=0;i<tags.size)
                              File file = new File(filePath);
                              System.out.println("-----file copy----->>>>"+file);
                              if(!file.exists()) {
                                  Toast.makeText(MainActivity.this, "File Not Found!", Toast.LENGTH_SHORT).show();
                                  return "";
                              }
                              ObjectMetadata objectMetadata = new ObjectMetadata();


                              Map<String,String> map=new HashMap<>();
                              map.put("unique",userId);
                              map.put("duration",duration);
                              map.put("videotitle",videoName);
                              map.put("videodescription",describtion);
                            // map.put("emailid","dhinakar@gmail.com");
                              map.put("hashtags",stringBuilder.toString());
                              objectMetadata.setUserMetadata(map);
                              // objectMetadata.addUserMetadata("video stream","video-stream-testing");
                              TransferObserver observer=transferUtility.upload("test", UUID.randomUUID().toString()+".mp4",file,objectMetadata);
                              observer.setTransferListener(new TransferListener() {

                                  @Override
                                  public void onStateChanged(int id, TransferState state) {

                                      if (state.COMPLETED.equals(observer.getState())) {

                                          new Handler(Looper.getMainLooper()).post(new Runnable() {
                                              @Override
                                              public void run() {
                                                 // Toast.makeText(MainActivity.this, "" + "Successfully uploaded", Toast.LENGTH_SHORT).show();
                                              }
                                          });
                                          Toast.makeText(MainActivity.this, "" + "Successfully uploaded", Toast.LENGTH_SHORT).show();
                                          System.out.println("---------->>>>"+state.toString());
                                          channel.invokeMethod("refreshTaps", "");

                                          //Toast.makeText(MainActivity.this, id, Toast.LENGTH_SHORT).show();
                                      }
                                  }


                                  @Override
                                  public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                                      int percentage = (int) (bytesCurrent / bytesTotal * 100);
                                  }


                                  @Override
                                  public void onError(int id, Exception ex) {

                                      Toast.makeText(MainActivity.this, "" + ex.getMessage(), Toast.LENGTH_SHORT).show();
                                  }
                              });
                          }catch (Exception e){
                              e.printStackTrace();
                              new Handler(Looper.getMainLooper()).post(new Runnable() {
                                  @Override
                                  public void run() {
                                      Toast.makeText(MainActivity.this, "" + "Something went wrong", Toast.LENGTH_SHORT).show();
                                  }
                              });

                          }



                          return "";
                      }

                      @Override
                      protected void onPostExecute(String token) {
                          channel.invokeMethod("refreshTaps", "");

                          //Log.i(TAG, "Access token retrieved:" + token);
                      }

                  };
                  task.execute();




              }

            }
              );
  }
}
