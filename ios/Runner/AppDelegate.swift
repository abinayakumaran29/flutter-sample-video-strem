import UIKit
import Flutter
import AWSCore
import AWSS3
enum ChannelName {
    static let battery = "samples.flutter.io/battery"
    static let charging = "samples.flutter.io/charging"
}

enum BatteryState {
    static let charging = "charging"
    static let discharging = "discharging"
}

enum MyFlutterErrorCode {
    static let unavailable = "UNAVAILABLE"
}

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate, FlutterStreamHandler {
    override func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
    ) -> Bool {
        GeneratedPluginRegistrant.register(with: self)
        getDataFromFlutter()
        return super.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
    public func onListen(withArguments arguments: Any?,
                         eventSink: @escaping FlutterEventSink) -> FlutterError? {
        return nil
    }
    func onCancel(withArguments arguments: Any?) -> FlutterError? {
        return nil
    }
    func getDataFromFlutter()  {
        guard let controller = window?.rootViewController as? FlutterViewController else {
            fatalError("rootViewController is not type FlutterViewController")
        }
        let uploadChannel = FlutterMethodChannel(name: ChannelName.battery,
                                                 binaryMessenger: controller.binaryMessenger)
        uploadChannel.setMethodCallHandler({
            [weak self] (call: FlutterMethodCall, result: FlutterResult) -> Void in
            result(FlutterMethodNotImplemented)
            guard call.method == "uploadVideoToIos" else {
                result(FlutterMethodNotImplemented)
                print("i am fonmu u")
                 guard call.method == "uploadVideoToIoss" else {
              return
                }
                 print("uploadVideoToIoss uploadVideoToIoss")
                let channels = FlutterBasicMessageChannel(
                                            name: "filePath",
                                            binaryMessenger: controller.binaryMessenger,
                                            codec: FlutterStringCodec.sharedInstance())
                                        // Send message to Dart and receive reply.
                                        channels.sendMessage("Hello, world") {(userData: Any?) -> Void in
                                            guard let userData = userData as? String else {
                                                return
                                            }
                                            let fullNameArr = userData.components(separatedBy: "^")
                                            let filePath = fullNameArr[0]
                                            let userId = fullNameArr[1]
                                            let fileUrl = URL(fileURLWithPath: filePath)
//                                            +videoTitle+" "+describtionn+" "+tagsString);
                                            let videoTitle = fullNameArr[2]
                                            let  describtionn = fullNameArr[3]
                                            let tagsString = fullNameArr[4]
                                            let duration = fullNameArr[5]
                                            self!.uploadVideoToAWSS3(fileUrl: fileUrl, controller: controller,userId:userId, videoTitle: videoTitle,describtionn:describtionn,tagsString:tagsString, duration: duration)
                                            print("fileURl da",fileUrl)
                                        }
                return
            }
            let channel = FlutterBasicMessageChannel(
                name: "filePath",
                binaryMessenger: controller.binaryMessenger,
                codec: FlutterStringCodec.sharedInstance())
            // Send message to Dart and receive reply.
            print("samma form")
            channel.sendMessage("Hello, world") {(userData: Any?) -> Void in
                print("ddsasdas userData",userData ?? "")
                guard let userData = userData as? String else {
                    return
                }
                let fullNameArr = userData.components(separatedBy: "^")
                let filePath = fullNameArr[0]
                let userId = fullNameArr[1]
                let fileUrl = URL(fileURLWithPath: filePath)
                
                let videoTitle = fullNameArr[2]
                let  describtionn = fullNameArr[3]
                let tagsString = fullNameArr[4]
                    let duration = fullNameArr[5]
                self!.uploadVideoToAWSS3(fileUrl: fileUrl, controller: controller,userId:userId, videoTitle: videoTitle,describtionn:describtionn,tagsString:tagsString, duration: duration)
                print("fileURl da",fileUrl)
            }
            
        })
        
        let chargingChannel = FlutterEventChannel(name: ChannelName.charging,
                                                  binaryMessenger: controller.binaryMessenger)
        chargingChannel.setStreamHandler(self)
    }
    func uploadVideoToAWSS3(fileUrl:URL,controller:FlutterViewController,userId:String,videoTitle:String,describtionn:String,tagsString:String,duration:String)
    {
        print("uploadVideoToAWSS3 sucess",fileUrl,userId,videoTitle,describtionn,tagsString,duration)
        let myIdentityPoolId = "test"
        let credentialsProvider:AWSCognitoCredentialsProvider = AWSCognitoCredentialsProvider(regionType:AWSRegionType.USEast1, identityPoolId: myIdentityPoolId)
        let configuration = AWSServiceConfiguration(region:AWSRegionType.USEast1, credentialsProvider:credentialsProvider)
        AWSServiceManager.default().defaultServiceConfiguration = configuration
        // Set up AWS Transfer Manager Request
        let S3BucketName = "test"
        let fileName = fileUrl.lastPathComponent
        let remoteName = fileName
        let uploadRequest = AWSS3TransferManagerUploadRequest()
        let key = remoteName + ".mp4"
      
         let data = AWSS3TransferUtilityMultiPartUploadExpression()
               let parameters = ["x-amz-meta-unique" : userId,"x-amz-meta-videotitle" :videoTitle,"x-amz-meta-videodescription":describtionn,"x-amz-meta-hashtags":tagsString,"x-amz-meta-duration":duration]
               for (key, metaValue) in parameters {
                if !metaValue.isEmpty {
                 data.setValue(metaValue, forRequestHeader: key)
                }
               }
//
//        data.setValue("x-amz-meta-videotitle", forRequestHeader: videoTitle)
//        if !describtionn.isEmpty  {
//        data.setValue("x-amz-meta-videodescription", forRequestHeader: describtionn)
//        }
//         if !tagsString.isEmpty {
//        data.setValue("x-amz-meta-hashtags", forRequestHeader: tagsString)
//        }
//        data.setValue("x-amz-meta-duration", forRequestHeader: duration)
//
       
        let transferManager = AWSS3TransferUtility.default()
//        transferManager.up
        transferManager.uploadUsingMultiPart(fileURL: fileUrl, bucket: S3BucketName, key: key, contentType: "video/mp4", expression: data, completionHandler: nil).continueWith { (task) -> AnyObject? in
                    if let error = task.error {
                        print("Upload failed with error: (\(error.localizedDescription))")
                        DispatchQueue.global(qos: .background).async {
        
                            // Background Thread
        
                            DispatchQueue.main.async {
                                // Run UI Updates
                                var alertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
                                var okAction = UIAlertAction(title: "Okay", style: UIAlertActionStyle.default) {
                                    UIAlertAction in
                                    NSLog("OK Pressed")
                                }
        
                                alertController.addAction(okAction)
        
                                self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
                            }
                        }
                    }
            print("Upload failed with exception (\(task.isCompleted ))",task.result?.status)
                    if task.result != nil {
//                        let sucessChannel = FlutterBasicMessageChannel(
//                            name: "success",
//                            binaryMessenger: controller.binaryMessenger,
//                            codec: FlutterStringCodec.sharedInstance())
//                        // Send message to Dart and receive reply.
//                        sucessChannel.sendMessage("Hello, world") {(filepath: Any?) -> Void in
//                        }
                        DispatchQueue.global(qos: .background).async {
        
                                          // Background Thread
        
                                          DispatchQueue.main.async {
                                              // Run UI Updates
                                              var alertController = UIAlertController(title: "Alert", message: "Successfully uploaded", preferredStyle: .alert)
                                              var okAction = UIAlertAction(title: "Okay", style: UIAlertActionStyle.default) {
                                                  UIAlertAction in
                                                  NSLog("OK Pressed")
                                              }
        
                                              alertController.addAction(okAction)
        
                                              self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
                                          }
                                      }
                    }
                    else {
                        print("Unexpected empty result.")
                        let sucessChannel = FlutterBasicMessageChannel(
                            name: "success",
                            binaryMessenger: controller.binaryMessenger,
                            codec: FlutterStringCodec.sharedInstance())
                        // Send message to Dart and receive reply.
                        sucessChannel.sendMessage("Hello, world") {(filepath: Any?) -> Void in
                        }
                        DispatchQueue.global(qos: .background).async {
        
                                           // Background Thread
        
                                           DispatchQueue.main.async {
                                               // Run UI Updates
                                               var alertController = UIAlertController(title: "Error", message: "Unexpected empty result.", preferredStyle: .alert)
                                               var okAction = UIAlertAction(title: "Okay", style: UIAlertActionStyle.default) {
                                                   UIAlertAction in
                                                   NSLog("OK Pressed")
                                               }
        
                                               alertController.addAction(okAction)
        
                                               self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
                                           }
                                       }
                    }
                    return nil
                }
    }
}
